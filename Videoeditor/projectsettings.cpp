#include "projectsettings.h"

ProjectSettings::ProjectSettings( VlcCutMain *vlcCut, Outromaker *outroMaker, QObject *parent) : QObject(parent),vlcCut(vlcCut),outroMaker(outroMaker)
{



}

void ProjectSettings::setProjectName(QString projectPath,QString projectName){
    file.setFileName(projectPath + "/" + projectName );

}

void ProjectSettings::setPtype(const ProjectType &ptype){
    this->ptype = ptype;
}

ProjectType ProjectSettings::getPtype(){
    return ptype;
}

void ProjectSettings::save(){
    if(vlcCut == 0)
        return;

    QList <SurfaceStruct> surfaceStruct = vlcCut->getSurfaces();
    QVector <double> openBracketVector = vlcCut->getOpenBracket()
            ,closeBracketVector = vlcCut->getCloseBracket();//current bracket, added from gui

    QString vlccutPath = vlcCut->getFilePath();
    OutroSettings outroSettings = outroMaker->getOutroSettings();

     file.open(QIODevice::WriteOnly);
     QXmlStreamWriter xmlWriter;
     xmlWriter.setDevice(&file);
     xmlWriter.setAutoFormatting(true);

     xmlWriter.writeStartDocument();
     xmlWriter.writeStartElement("project");
     xmlWriter.writeAttribute("projectType",QString::number((int)ptype));
     xmlWriter.writeStartElement("outro");
     xmlWriter.writeAttribute("template",outroSettings.templateNumber);
     xmlWriter.writeAttribute("music",outroSettings.musicPath);
     xmlWriter.writeAttribute("duration",QString::number(outroSettings.duration));
     xmlWriter.writeAttribute("quality",outroSettings.quality);

     for(int i = 0; i < outroSettings.path.size() && i < outroSettings.title.size() && outroSettings.color.size(); i++){
         xmlWriter.writeStartElement("videoParam");
         /*add one attribute and its value*/
         xmlWriter.writeAttribute("title",outroSettings.title.at(i));
         /*add one attribute and its value*/
         xmlWriter.writeAttribute("color",outroSettings.color.at(i));
         /*add one attribute and its value*/
         xmlWriter.writeAttribute("startTime",outroSettings.startTime.at(i));
         /*add character data to tag student */
         xmlWriter.writeAttribute ("path",outroSettings.path.at(i));
         /*close tag student  */
         xmlWriter.writeEndElement();

     }
     foreach (QString str, outroSettings.logoChecked) {
         xmlWriter.writeStartElement("logo");
         xmlWriter.writeAttribute("path",str);
         xmlWriter.writeEndElement();
     }
     /*end tag outro*/
     xmlWriter.writeEndElement();


     xmlWriter.writeStartElement("vlccut");
     xmlWriter.writeAttribute("path",vlccutPath);
     xmlWriter.writeAttribute("length",QString::number(vlcCut->getVideoLength()));
     foreach (SurfaceStruct tmp, surfaceStruct) {
         xmlWriter.writeStartElement("surface");
         xmlWriter.writeAttribute("x",QString::number(tmp.previewrect.x));
         xmlWriter.writeAttribute("y",QString::number(tmp.previewrect.y));
         xmlWriter.writeAttribute("width",QString::number(tmp.previewrect.width));
         xmlWriter.writeAttribute("height",QString::number(tmp.previewrect.height));
         xmlWriter.writeAttribute("angle",QString::number(tmp.angle));
         xmlWriter.writeAttribute("startTime",QString::number(tmp.startTime));
         xmlWriter.writeAttribute("stopTime",QString::number(tmp.stopTime));
         xmlWriter.writeAttribute("type",QString::number(tmp.type));
         switch (tmp.type) {
         case 0://image
             xmlWriter.writeAttribute("imgPath",tmp.img.path);
             break;
         case 1://text
             xmlWriter.writeAttribute("text",tmp.text.text);
             xmlWriter.writeAttribute("textColor",tmp.text.color);
             xmlWriter.writeAttribute("textFont",tmp.text.fontName);
             xmlWriter.writeAttribute("textHighlightState",QString::number(tmp.text.highLightText));
             xmlWriter.writeAttribute("textHighlightColor",tmp.text.highLightTextColor);

             break;
         case 2://blur
            xmlWriter.writeAttribute("radius",tmp.blur.previewRadius);
             break;
         case 3://music
            xmlWriter.writeAttribute("musicPath",tmp.music.path);
            xmlWriter.writeAttribute("length",QString::number(tmp.music.length));
            xmlWriter.writeAttribute("volume",tmp.music.volume);
             break;
         }
         xmlWriter.writeEndElement();
     }

     foreach (double value, openBracketVector) {
         xmlWriter.writeStartElement("openBracket");
         xmlWriter.writeAttribute("value",QString::number(value));
         xmlWriter.writeEndElement();
     }
     foreach (double value, closeBracketVector) {
         xmlWriter.writeStartElement("closeBracket");
         xmlWriter.writeAttribute("value",QString::number(value));
         xmlWriter.writeEndElement();
     }
     /*end tag vlccut*/
     xmlWriter.writeEndElement();
     //end tag project
     xmlWriter.writeEndElement();

     xmlWriter.writeEndDocument();
     file.close();
}

void ProjectSettings::saveAs(QString fileName){

    QString oldFileName = file.fileName();
    file.setFileName(fileName);
    save();
    file.setFileName(oldFileName);
}

void ProjectSettings::load(QString projectPath){
    QFile in(projectPath);
    qDebug() << "load" << projectPath;

    if (in.open(QIODevice::ReadOnly)) {
        qDebug() << "open";
        OutroSettings outroSettings;
        QList <SurfaceStruct> surfaceStruct;
        QVector <double> openBracketVector,closeBracketVector;
        QString videoLength;
        QString vlccutPath;

        QXmlStreamReader xml(&in);
        while(!xml.atEnd() && !xml.hasError())
            {
                QXmlStreamReader::TokenType token = xml.readNext();
                if(token == QXmlStreamReader::StartDocument)
                {
                    continue;
                }
                if(token == QXmlStreamReader::StartElement)
                {
                    if(xml.name() == "project"){
                        ptype = (ProjectType)xml.attributes().value("projectType").toInt();
                    }
                    if(xml.name() == "outro"){
                        outroSettings.templateNumber = xml.attributes().value("template").toString();
                        outroSettings.musicPath = xml.attributes().value("music").toString();
                        outroSettings.duration = xml.attributes().value("duration").toInt();
                        outroSettings.quality = xml.attributes().value("quality").toString();
                    }
                    if(xml.name() == "videoParam"){
                        outroSettings.title.push_back(xml.attributes().value("title").toString());
                        outroSettings.color.push_back(xml.attributes().value("color").toString());
                        outroSettings.startTime.push_back(xml.attributes().value("startTime").toString());
                        outroSettings.path.push_back(xml.attributes().value("path").toString());
                    }
                    if(xml.name() == "logo"){
                        outroSettings.logoChecked << xml.attributes().value("path").toString();
                    }
                    if(xml.name() == "vlccut"){
                        vlccutPath = xml.attributes().value("path").toString();
                        videoLength = xml.attributes().value("length").toString();
                    }
                    if(xml.name() == "surface"){
                        SurfaceStruct tmp;
                        tmp.previewrect.x = xml.attributes().value("x").toInt();
                        tmp.previewrect.y = xml.attributes().value("y").toInt();
                        tmp.previewrect.width = xml.attributes().value("width").toInt();
                        tmp.previewrect.height = xml.attributes().value("height").toInt();
                        tmp.angle = xml.attributes().value("angle").toInt();
                        tmp.startTime = xml.attributes().value("startTime").toFloat();
                        tmp.stopTime = xml.attributes().value("stopTime").toFloat();
                        tmp.type = xml.attributes().value("type").toInt();
                        switch (tmp.type) {
                        case 0://image
                            tmp.img.path = xml.attributes().value("imgPath").toString();
                            break;
                        case 1://text
                            tmp.text.text = xml.attributes().value("text").toString();
                            tmp.text.color = xml.attributes().value("textColor").toString();
                            tmp.text.fontName = xml.attributes().value("textFont").toString();
                            tmp.text.highLightText = xml.attributes().value("textHighlightState").toShort();
                            tmp.text.highLightTextColor = xml.attributes().value("textHighlightColor").toString();

                            break;
                        case 2://blur
                           tmp.blur.previewRadius = xml.attributes().value("radius").toString();
                            break;
                        case 3://music
                           tmp.music.path = xml.attributes().value("musicPath").toString();
                           tmp.music.length = xml.attributes().value("length").toInt();
                           tmp.music.volume = xml.attributes().value("volume").toString();
                            break;
                        }
                        surfaceStruct << tmp;
                    }
                    if(xml.name() == "openBracket"){
                        openBracketVector.push_back(xml.attributes().value("value").toFloat());
                    }
                    if(xml.name() == "closeBracket"){
                        closeBracketVector.push_back(xml.attributes().value("value").toFloat());
                    }
                }
            }
        outroMaker->loadNewOutroSettings(outroSettings);
        vlcCut->loadNewSettings(surfaceStruct,vlccutPath,videoLength,openBracketVector,closeBracketVector);
    }

}

ProjectSettings::~ProjectSettings(){

}
