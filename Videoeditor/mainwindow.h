#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "swf1080/swf1080title.h"
#include "swf1080/swf1080intro.h"
#include "swf1080/previewwidget.h"
#include <QQuickView>
#include <QtPlugin>
#include <vlc-cut/vlc-cut.h>
#include <outro-maker/outromaker.h>
#include <outro-maker/videoplayerparam.h>

#include <QDialog>
#include <QThread>
#include <QMouseEvent>
#include <QMainWindow>
#include "projectdialog.h"
#include "projectsettings.h"
#include "mergefiles.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QString defaultProjectPath;

    QQuickView *vlccutView,*outroMakerView;
    QWidget *vlccutContainer, *outroMakerContainer;
    VlcCutMain *vlccut;
    Outromaker *outromaker;
    QThread vlccutThread,outromakerThread;

    ProjectDialog *pd;
    ProjectSettings *ps;

    MergeFiles *mf;

    QObject *vlccutMainPage;
    void onUpButtonClicked(QPushButton *buttonClicked,QPushButton *firstButton, QPushButton *secondButton);
    void loadNewProject(QString projectName,bool newProject);
    void checkUpButton(ProjectType pType);
    void mergeFin();
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_twIntroTitle_tabBarClicked(int index);
    void on_exitButton_clicked();
    void on_saveButton_clicked();
    void on_saveAsButton_clicked();
    void on_loadButton_clicked();
    void on_localFileButton_clicked();
    void on_youtubeButton_clicked();
    void on_screenRecording_clicked();
    void on_recordButton_clicked();
    void setSize(QVariant width, QVariant height);
    void setVideoName(QString fileName);
    void dialogFinished();
    void outroVideoActivate();
    void vlccutVideoActivate();
    void introActivate();
    void titleActivate();
    void outroVideoDisactivate();
    void vlccutVideoDisactivate();
    void introDisactivate();
    void titleDisactivate();

private:
    Ui::MainWindow *ui;
    Swf1080intro *m_swfControlsIntro;
    Swf1080title *m_swfControlsTitle;
    PreviewWidget *m_axWidget;
    FlListView *m_lvFolders;

    void setStyle();
};

#endif // MAINWINDOW_H
