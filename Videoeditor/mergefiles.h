#ifndef MERGEFILES_H
#define MERGEFILES_H

#include <QObject>
#include <QFile>
#include <QDir>
#include <QStringList>
class MergeFiles : public QObject
{
    Q_OBJECT
    QString titlePath,introPath,mainVideo,outroPath;
    QFile mergeFile;
    void ffmpegProc(QStringList param);
public:
    explicit MergeFiles(QObject *parent = 0);

signals:
    void mergeFinished();
public slots:
    void setIntroPath(QString path);
    void setTitlePath(QString path);
    void setMainPath(QString path);
    void setOutroPath(QString path);
    void merge();

};

#endif // MERGEFILES_H
