#ifndef INPUTURL_H
#define INPUTURL_H

#include <QObject>
#include <QThread>

#include "filedownloader.h"

struct Param {
    QString id;
    QString ext;
    QString quality;
    QString musicId;
    QString musicExt;
};




class YoutubeAction : public QObject
{
    Q_OBJECT

    QString youtubeProgram = "youtube-dl.exe";
    QString ffmpegProgram = "ffmpeg.exe";

    QString getYoutubeRealUrl(QString youtubeUrl);
    QString getYoutubeRealUrl(QString youtubeUrl,QString quality);
    void getYoutubeFileExt(QString youtubeUrl);
    void getYoutubeQualityList(QString youtubeUrl);
    void downloadYoutubeUrl(QString youtubeUrl, QString quality);
    void mergeYoutubeUrl(QString videoPath, QString audioPath);


    QString downloadedFileName,mDownloadedFileName;

    QVector <Param> params ;
    FileDownloader *fileDownloader;
    QThread downLoaderThread;
public:
    explicit YoutubeAction(QObject *parent = 0);
    ~YoutubeAction();
    Q_INVOKABLE void getRealUrl(QString youtubeUrl);
    Q_INVOKABLE void getFileExt(QString youtubeUrl);
    Q_INVOKABLE void getQualityList(QString youtubeUrl);
    Q_INVOKABLE void downloadYoutube(QString youtubeUrl,QString quality);
signals:
    void downloadFinished(QString filePath);
    void realUrlWasFound(QString realUrl);
    void fileExtWasFound(QString youtubeFileExt);
    void qualityListWasFound(QStringList qualityList);
    void progress(QVariant value,QVariant maxValue);
private slots:
    void mergeFiles(QString videoPath,QString audioPath);
};


#endif // INPUTURL_H
