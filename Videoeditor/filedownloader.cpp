#include "filedownloader.h"
#include <QTime>

FileDownloader::FileDownloader(QObject *parent) :
 QObject(parent)
{
 }

void FileDownloader::init(){
    WebCtrl = new QNetworkAccessManager(this);
    connect(WebCtrl,&QNetworkAccessManager::finished,
     this, &FileDownloader::fileDownloaded);

    m_WebCtrl = new QNetworkAccessManager(this);
    connect(m_WebCtrl,&QNetworkAccessManager::finished,
     this, &FileDownloader::mfileDownloaded);
}

void FileDownloader::downloadUrl(QUrl url, QString fileName){
    qDebug() << "start download";
    QNetworkRequest request(url);
    pReply = WebCtrl->get(request);
    connect(pReply,&QNetworkReply::downloadProgress,this,&FileDownloader::progress);
    connect(pReply,&QNetworkReply::readyRead,this,&FileDownloader::saveFile);

    connect(pReply, static_cast<void(QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
          [=](QNetworkReply::NetworkError code){
        qDebug() << "network error" << code << pReply->errorString();
        QFile log(QTime::currentTime().toString("hh:mm:ss") + "_downloadLog.log");
        if(log.open(QIODevice::WriteOnly)){
            QTextStream out(&log);
            out << code << "\t" << pReply->errorString() << endl;
            log.close();
        }
    });

    connect(pReply, &QNetworkReply::sslErrors,
            [=](QList<QSslError> sslErrors){

                 QFile log(QTime::currentTime().toString("hh:mm:ss") + "_downloadLog.log");
                    if(log.open(QIODevice::WriteOnly)){
                    QTextStream out(&log);
                    foreach (const QSslError &error, sslErrors){
                        qDebug() << "ssl error" << error.errorString();
                        out << "SSL error: " << error.errorString() << endl;
                    }

                    log.close();
                }

            });


    downloadFile.setFileName(fileName);
    downloadFile.open(QIODevice::WriteOnly);
    downloadType = DownloadType::VIDEO;
}

void FileDownloader::downloadUrlDASH(QUrl vurl, QUrl murl, QString vFileName, QString mFileName){
    downloadUrl(vurl,vFileName);

    QNetworkRequest request(murl);
    mpReply = m_WebCtrl->get(request);
    connect(mpReply,&QNetworkReply::readyRead,this,&FileDownloader::savemFile);
    mdownloadFile.setFileName(mFileName);
    mdownloadFile.open(QIODevice::WriteOnly);
    downloadType = DownloadType::VIDEO1080;
}

void FileDownloader::saveFile(){
    downloadFile.write(pReply->readAll());
}

void FileDownloader::savemFile(){
    mdownloadFile.write(mpReply->readAll());
}

FileDownloader::~FileDownloader() { }

void FileDownloader::fileDownloaded(QNetworkReply* pReply) {
 disconnect(this->pReply,&QNetworkReply::downloadProgress,this,&FileDownloader::progress);
 disconnect(this->pReply,&QNetworkReply::readyRead,this,&FileDownloader::saveFile);

 if(downloadType == DownloadType::VIDEO)
    emit downloaded(downloadFile.fileName());
 else{
     if(!mdownloadFile.isOpen())
         emit downloadedDASH(downloadFile.fileName(),mdownloadFile.fileName());
 }

 downloadFile.close();
 pReply->deleteLater();
}


void FileDownloader::mfileDownloaded(QNetworkReply* pReply) {
    disconnect(this->mpReply,&QNetworkReply::readyRead,this,&FileDownloader::savemFile);

    if(!downloadFile.isOpen())
        emit downloadedDASH(downloadFile.fileName(),mdownloadFile.fileName());

    mdownloadFile.close();
    pReply->deleteLater();
}
