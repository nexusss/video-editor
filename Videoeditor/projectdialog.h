#ifndef PROJECTDIALOG_H
#define PROJECTDIALOG_H

#include <QQuickView>
#include <QDialog>
#include <QLayout>
#include <youtubeAction.h>
#include <QQuickItem>
enum class ProjectType{LOCALVIDEO,YOUTUBEVIDEO,SCREENRECORDER,LOADPROJECT};//type of project, uses for upper button

class ProjectDialog : public QDialog
{
    Q_OBJECT
    QQuickView *popupView;
    QWidget *popupContainer;
    int m_nMouseClick_X_Coordinate;//for drag and drop
    int m_nMouseClick_Y_Coordinate;
    QString projectName;
    QString videoPath;
    QString projectFile;
    ProjectType projectType = ProjectType::LOCALVIDEO;

public:
    explicit ProjectDialog(QString projectPath,QWidget *parent = 0);
    ProjectType getProjectType();
    QString getPath();
    QString getProjectName();
    QString getProjectFile();
private slots:
    void openLocalVideo(QString path);
    void openYoutubeVideo(QString path);
    void startScreenRecord();
    void setProjectName(QString projectName);
    void setProjectFile(QString projectFile);
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
};


#endif // PROJECTDIALOG_H
