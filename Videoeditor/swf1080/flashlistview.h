#ifndef FOLDERVIEW_H
#define FOLDERVIEW_H

#include "flashlistmodel.h"
#include <QListView>
#include <QFileInfo>
#include <QDebug>

class FlListView : public QListView
{
    Q_OBJECT

public:

    FlListView(QString root, QWidget *parent = 0);

public slots:

    void on_clicked(const QModelIndex &index);

signals:

    void setStateIdle();
    void passFileInfo(QFileInfo);

private:

};

#endif // FOLDERVIEW_H
