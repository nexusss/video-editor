#include "flashlistview.h"

FlListView::FlListView(QString root, QWidget *parent):
    QListView(parent)
{
    setModel(new FlashListModel(root, this));
    connect(this,
            static_cast<void (FlListView::*) (const QModelIndex &) > (&FlListView::clicked),
            &FlListView::on_clicked
            );
}

void FlListView::on_clicked(const QModelIndex &index)
{
    emit setStateIdle();
    QDir dir (dynamic_cast<FlashListModel *>(model())->path());
    QFileInfoList entries = dir.entryInfoList(QDir::AllDirs | QDir::NoDotAndDotDot);
    QDir subDir (entries.value(index.row()).filePath());
    QFileInfoList subDirEntries = subDir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot);
    for (auto info: subDirEntries) {
        /* Returns the first file with 'swf' extension */
        if (info.isFile() && info.suffix() == "swf") {
            emit passFileInfo(info);
        }
    }
}



