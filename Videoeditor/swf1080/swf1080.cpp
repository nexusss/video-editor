#include "swf1080.h"
#include "ui_swf1080.h"

Swf1080::Swf1080(QWidget *parent) :
    QWidget(parent)

{

    ui = new Ui::Swf1080;
    m_sizes.insert("1080p", QSize(1980, 1080));
    m_sizes.insert("720p", QSize(1280, 720));
    m_sizes.insert("540p", QSize(960, 540));
    m_sizes.insert("360p", QSize(640, 360));

    ui->setupUi(this);
    readSettings();
    m_lvFolders = new FlashListView(m_root, this);

}

Swf1080::~Swf1080()
{
    delete ui;
}

bool Swf1080::readSettings()
{

    if (!QFile(QApplication::applicationDirPath() + "/settings.ini").exists()) {

        throw QString("No settings.ini file found <br>in the application directory");
    }

    m_settings = new QSettings (QApplication::applicationDirPath() + "/settings.ini", QSettings::IniFormat);
    if (m_settings->status() == QSettings::FormatError) {
        throw QString("Wrong settings.ini format");
    }

    if (m_settings->status() == QSettings::AccessError) {
        throw QString("settings.ini access error");
    }

    m_root = QApplication::applicationDirPath() + "/" + m_settings->value("Root").toString();
    m_duration = m_settings->value("Duration").toInt();
    m_width = m_settings->value("Width").toInt();
    m_height = m_settings->value("Height").toInt();
    m_fps = 25;

    if (!m_settings->value("FPS").isNull() && m_settings->value("FPS").isValid()) {
        m_fps = m_settings->value("FPS").toDouble();
    }

    m_xmlFileName = m_settings->value("Xml_File_Name").toString();
    m_fillWhite = m_settings->value("Fill_White").toBool();
    return true;
}
