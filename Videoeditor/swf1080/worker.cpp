#include "worker.h"

Worker::Worker(QWidget *parent) : QWidget(parent)
{

    processor = new ImageProcessor(this);

    /* Set hidden window */

    hiddenWindow = new QAxWidget (this);

    hiddenWindow->setWindowFlags(Qt::Window);

    hiddenWindow->setGeometry(0, GetSystemMetrics(SM_CYVIRTUALSCREEN), 1920, 1080);

    hiddenWindow->show();

    timer = new QTimer(this);

    /* Connect timer */

    connect(timer, &QTimer::timeout, this, &Worker::on_timer);

    connect (this, &Worker::stopTimer, timer, &QTimer::stop);

    connect(processor, &ImageProcessor::finished, this, &Worker::on_finished);

    connect (this, &Worker::release, processor, &ImageProcessor::on_Release);

    connect (this, &Worker::start, this, &Worker::on_Capture);

}


Worker::~Worker() {

    emit release();

}

void Worker::init(SwfSettings swfSettings)
{

    m_swfSettings = swfSettings;

    m_swfFile = QFileInfo(swfSettings.WorkingFolder);

    /* Set timer interval */

    m_interval = 1000. / m_swfSettings.FrameRate;

}

void Worker::run()
{
    //QFuture<void> f = QtConcurrent::run(this, &Worker::on_Capture);
    on_Capture();
}

void Worker::on_Capture()
{

    Q_ASSERT(m_interval > 10);

    counter = 0;

    QFile logFile("mainwindow.log");

    logFile.open(QIODevice::ReadWrite);

    QTextStream str;

    str.setDevice(&logFile);

    QFileInfo tempFile(QApplication::applicationDirPath() + "/Output/~temp.mov");

    QFile::remove(tempFile.absoluteFilePath());

    try {

        m_swfSettings.WorkingFolder = m_swfFile.path();

        processor->initialize(m_swfSettings);

    } catch (std::exception &e) {
        str << "Mainwindow:" << e.what();
    }

    hiddenWindow->clear();

    qDebug() << "Path " << m_swfFile.path() << m_swfFile.filePath();

    hiddenWindow->setControl(QString::fromUtf8("{d27cdb6e-ae6d-11cf-96b8-444553540000}"));

    hiddenWindow->dynamicCall("SetBase(QString)", "file:///" + m_swfFile.path() + "/");

    hiddenWindow->dynamicCall("LoadMovie(long,string)", 0, m_swfFile.filePath());

    hiddenWindow->setMaximumSize(m_swfSettings.Size);

    hiddenWindow->setMinimumSize(m_swfSettings.Size);

    timer->setInterval(m_interval);

    //    m_timerLimit = (double) m_swfSettings.Duration / (double) m_interval;

//    m_timerLimit = (double) m_swfSettings.Duration;

//    qDebug() << m_timerLimit;

    timeBegin = std::chrono::steady_clock::now();

    timer->start();
    qDebug() << "timer start";
    logFile.close();

}

void Worker::on_timer()
{
//    qDebug() << "om timer";

    using namespace std::chrono;

    auto timeElipsed = duration_cast<milliseconds>(steady_clock::now() - timeBegin);

    if (timeElipsed.count() >= (double) m_swfSettings.Duration) {

        //        QFuture<void> f = QtConcurrent::run(processor, &ImageProcessor::save);

        processor->save();

        //        f.waitForFinished();

        emit stopTimer();

        emit captureFinished();

        return;

    }

    processor->capture(hiddenWindow->grab().toImage());

    ++counter;

}

void Worker::on_finished(bool result)
{

    qDebug() << "worker on finished";
    emit finished(result);

    hiddenWindow->clear();
}
