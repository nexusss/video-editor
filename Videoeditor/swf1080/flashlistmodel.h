#ifndef FLASHLISTMODEL_H
#define FLASHLISTMODEL_H

#include "swflib_global.h"
#include <QAbstractListModel>
#include <QDir>
#include <QFileInfo>
#include <QModelIndex>
#include <QDebug>

class FlashListModel : public QAbstractListModel
{
    Q_OBJECT

public:

    FlashListModel(QString path, QObject *parent);

    virtual QVariant data(const QModelIndex &index, int role) const;

    virtual int rowCount(const QModelIndex &parent) const;

    QString path() const { return m_path; }

private:

    QString m_path;

};

#endif // FLASHLISTMODEL_H
