#ifndef WORKER_H
#define WORKER_H

#include <imageprocessor.h>
#include <swfsettings.h>

#include <QObject>
#include <QWidget>
#include <QAxWidget>
#include <QTimer>
#include <QFile>
#include <QFileInfo>
#include <QtConcurrent/QtConcurrent>

#include <Windows.h>
#include <Wingdi.h>
#include <ObjIdl.h>
#include <gdiplus.h>

#include <chrono>

class Worker : public QWidget
{

    Q_OBJECT

public:

    explicit Worker(QWidget *parent);

    void init(SwfSettings swfSettings);

    void run();

    ~Worker();

signals:

public slots:

    void on_Capture();

    void on_timer();

    void on_finished(bool result);

private:

    uint counter { 0 };

    SwfSettings m_swfSettings;

    QAxWidget *hiddenWindow;

    ImageProcessor *processor;

    QTimer *timer;

    QFileInfo m_swfFile;

    double m_interval;

    uint m_timerLimit { 0 };

    /* Start time point of capture timer */
    std::chrono::steady_clock::time_point timeBegin;


signals:

    void start();

    void finished(bool);

    void release();

    void stopTimer();

    void captureFinished();

};

#endif // WORKER_H
