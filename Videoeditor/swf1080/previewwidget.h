#ifndef PREVIEWWIDGET_H
#define PREVIEWWIDGET_H

#include "swflib_global.h"
#include <QAxWidget>
#include <QFileInfo>

class PreviewWidget : public QAxWidget
{
    Q_OBJECT

public:

    PreviewWidget(QWidget *parent = 0);

public slots:

    void on_loadMovie(const QFileInfo &fileInfo);

    void on_clear() { this->clear(); }

};

#endif // PREVIEWWIDGET_H
