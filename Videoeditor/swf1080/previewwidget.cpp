#include "previewwidget.h"

PreviewWidget::PreviewWidget(QWidget *parent):
    QAxWidget(parent)
{

}

void PreviewWidget::on_loadMovie(const QFileInfo &fileInfo)
{
    setControl(QString::fromUtf8("{d27cdb6e-ae6d-11cf-96b8-444553540000}"));
    dynamicCall("SetBase(QString)", "file:///" + fileInfo.path() + "/");
    dynamicCall("SetVariable(string, Boolean)", "Loop", false);

    /* Alternative setControl
    setControl(QString::fromUtf8("Shockwave Flash Object"));
    **/

    dynamicCall("LoadMovie(long,string)", 0, fileInfo.filePath());
}
