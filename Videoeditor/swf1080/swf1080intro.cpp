#include "Swf1080intro.h"
#include "ui_Swf1080intro.h"

Swf1080intro::Swf1080intro(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Swf1080intro)
{
    float k = 16 / 9.0;

    m_sizes.insert("1080", QSize(1080 * k, 1080)) ;
    m_sizes.insert("720", QSize(720 * k, 720)) ;
    m_sizes.insert("480", QSize(540 * k, 540)) ;
    m_sizes.insert("360", QSize(360 * k, 360)) ;
    m_sizes.insert("240", QSize(240 * k, 240)) ;
    m_sizes.insert("144", QSize(144 * k, 144)) ;

    ui->setupUi(this);

    readSettings();

    worker = new Worker (this);

    m_lvFolders = new FlListView(m_root, this);
    m_lvFolders->setGeometry(ui->frFoldersView->geometry());

    setStyle();

    /* Set model and view for font size combo boxes */

    sizeModel_1 = new QStringListModel(this);

    QStringList sizes;

    for (int i = 8; i < 160; i = i + 2) {
        sizes.append(QString::number(i));
    }

    sizeModel_1->setStringList(sizes);

    ui->cbSize_1->setModel(sizeModel_1);

    /* Export combo box */

    exportModel = new QStringListModel(this);

    exportModel->setStringList(m_resolutions);

    ui->cbExport->setModel(exportModel);

    /* Set xml parser */

    parser = new XmlParser(this);

    connect (this, &Swf1080intro::doRead, parser, &XmlParser::read);

    connect (this, &Swf1080intro::setFile, parser, &XmlParser::on_setFile);

    /* Set texts in GUI from XML */

    connect (parser, &XmlParser::displayText, this, [&](uint i, QString text)
    {
        if ( i == 1 ) {
            ui->leText_1->setText(text);
        }
    });

    /* Set text size in GUI from XML */

    connect (parser, &XmlParser::displaySize, this, [&] (uint i, QString size)
    {
        if ( i == 1 ) {
            ui->cbSize_1->setCurrentIndex(ui->cbSize_1->findText(size));
        }
    });

    /* Set text font in GUI from XML */

    connect (parser, &XmlParser::displayFont, this, [&] (uint i, QString font)
    {
        if ( i == 1 ) {
            ui->fontComboBox_1->setCurrentIndex(ui->fontComboBox_1->findText(font));;
        }
    });

    /* Set text color in GUI from XML */

    connect (parser, &XmlParser::displayColor, this, [&] (uint i, QString color)
    {
        if ( i == 1 ) {
            currentColor_1.setNamedColor(color.replace("0x", "#", Qt::CaseInsensitive));
        }
    });

    connect (this, &Swf1080intro::changeText, parser, &XmlParser::on_changeText);

    /* Text size in GUI changed, change XML */

    connect (this, &Swf1080intro::changeTextSize, parser, &XmlParser::on_changeTextSize);

    connect (this, &Swf1080intro::changeTextColor, parser, &XmlParser::on_changeTextColor);

    /* Text size in GUI changed, change XML */

    connect (this, &Swf1080intro::changeTextFont, parser, &XmlParser::on_changeTextFont);

    /* Selected text entry changed, get values for it */

    connect (this, &Swf1080intro::getValues, parser, &XmlParser::on_getValues);

    connect (this, &Swf1080intro::changeLoop, parser, &XmlParser::on_changeLoop);


    /* Done timer */

    doneTimer = new QTimer(this);

    doneTimer->setInterval(3000);

    connect(doneTimer, &QTimer::timeout, [&]()
    {
        setStateIdle();
        doneTimer->stop();
    });

    connect(worker, &Worker::finished, this, &Swf1080intro::on_finished);

    connect(worker, &Worker::captureFinished, this, &Swf1080intro::on_captureFinished);

    connect(m_lvFolders, &FlListView::passFileInfo, this, &Swf1080intro::on_passFileInfo);

    ui->cbExport->setCurrentIndex(4);

    /* Set application priority */

#ifdef Q_OS_WIN
    /* Windows code here */
    auto processHandle = GetCurrentProcess();
    SetPriorityClass(processHandle, REALTIME_PRIORITY_CLASS);
#endif

#ifdef Q_OS_IOS
    /* IOS code here */
#endif
}

Swf1080intro::~Swf1080intro()
{
    delete ui;
    emit release();
}

bool Swf1080intro::readSettings()
{

    if (!QFile(QApplication::applicationDirPath() + "/settings.ini").exists()) {
        throw QString("No settings.ini file found <br>in the application directory");
    }

    m_settings = new QSettings (QApplication::applicationDirPath() + "/settings.ini", QSettings::IniFormat);
    if (m_settings->status() == QSettings::FormatError) {
        throw QString("Wrong settings.ini format");
    }

    if (m_settings->status() == QSettings::AccessError) {
        throw QString("settings.ini access error");
    }

    m_root = QApplication::applicationDirPath() + "/" + m_settings->value("RootIntro").toString();
    m_duration = m_settings->value("Duration").toInt();
    m_width = m_settings->value("Width").toInt();
    m_height = m_settings->value("Height").toInt();
    m_fps = 25;

    if (!m_settings->value("FPS").isNull() && m_settings->value("FPS").isValid()) {
        m_fps = m_settings->value("FPS").toDouble();
    }

    m_xmlFileName = m_settings->value("Xml_File_Name").toString();
    m_fillWhite = m_settings->value("Fill_White").toBool();

    return true;
}

void Swf1080intro::on_pbCancel_clicked()
{
    qApp->exit();
}



void Swf1080intro::on_pbColor_1_clicked()
{
    QColorDialog colorDialog;
    colorDialog.setCurrentColor(currentColor_1);
    auto color = colorDialog.getColor();
    emit changeTextColor(1, color.name());
}

void Swf1080intro::on_cbSize_1_activated(const QString &arg1)
{
    emit changeTextSize(1, arg1);
}

void Swf1080intro::on_fontComboBox_1_activated(const QString &arg1)
{
    emit changeTextFont(1, arg1);
}


void Swf1080intro::on_leText_1_textChanged(const QString &arg1)
{
    emit changeText(1, arg1);
}


void Swf1080intro::on_passFileInfo(const QFileInfo &info)
{

    qDebug() << "clicked";

    setStateIdle();
    emit clear();

    m_fileInfo = info;

    if (!m_fileInfo.exists()) {
        qDebug() << "no file info";
        ui->pbExport->setEnabled(false);
        return;
    }

    ui->pbExport->setEnabled(true);

    emit loadMovie(m_fileInfo);

    /* Start parsing of yourtext.xml */

    emit setFile(m_fileInfo.path() + "/" + m_xmlFileName);
    emit doRead();
}


void Swf1080intro::setStyle()
{
//    QString style {
//        "Swf1080intro { background: #2E3A46; } "
//    };

//    this->setStyleSheet(style);
}

void Swf1080intro::setQuality(QVariant width, QVariant height){
    qDebug() << "setquality" << height.toString();
    ui->cbExport->setCurrentText(height.toString());
}

void Swf1080intro::on_pbAddLogo_clicked()
{
    auto logoFileName = QFileDialog::getOpenFileName(nullptr,
                                                     tr("Open logo"),
                                                     QApplication::applicationDirPath() + "/Logo",
                                                     tr("Logo files (*.png)")
                                                     );

    /* No file selected */
    if (logoFileName == "") {
        return;
    }

    QFile logoFile(logoFileName);

    auto index = m_lvFolders->currentIndex();


    if (!index.isValid()) {
        qDebug() << "Invalid index";
        return;
    }

    /* Check whether IMAGE folder exists */

    if (!QFile::exists(m_fileInfo.absolutePath() + "/IMAGE")) {
        qDebug() << "No IMAGE folder";
        return;
    }

    QString targetFileName { m_fileInfo.absolutePath() + "/IMAGE/logo.png" };

    /* If logo.png exists */

    if (QFile::exists(targetFileName)) {
        QFile::remove(targetFileName);
    }
    logoFile.copy(targetFileName);
}

void Swf1080intro::on_pbAddAudio_clicked()
{
    auto audioFileName = QFileDialog::getOpenFileName(nullptr,
                                                   tr("Open audio"),
                                                   QApplication::applicationDirPath() + "/Sounds",
                                                   tr("Audio files (*.mp3)")
                                                   );
    if (audioFileName == "") {
        return;
    }
    m_audioFileName = audioFileName;
}

void Swf1080intro::on_pbExport_clicked()
{

    try {
        if(!parser->valid()) {
            throw std::runtime_error("Error!!! No xml file");
        }
        if (parser->getDuration() > 0) {
            m_duration = parser->getDuration();
        } else {
            throw std::runtime_error("Error!!! Wrong movie duration");
        }
        if (m_fps < 0 || m_fps > 100) {
            throw std::runtime_error(QString("Error!!! Wrong fps (%1)").arg(m_fps).toStdString());
        }
    } catch (std::exception &e) {
        setStateError();
        ui->lbState->setText(e.what());
        return;
    }


    setStateProcessing();
    time1 = std::chrono::steady_clock::now();
    emit changeLoop(false);
    doneTimer->stop();

    /* ... */

    auto sz = m_sizes.value(ui->cbExport->currentText());

    /* Create Output directory if not exists */

    if (!QDir(QApplication::applicationDirPath() + "/Output1").exists()) {
        QDir().mkdir(QApplication::applicationDirPath() + "/Output1");
    }

    m_outFileName = QFileInfo(QApplication::applicationDirPath()
                              + "/Output1/temp_"
                              + QString::number(QDateTime::currentMSecsSinceEpoch())
                              + ".mp4").absoluteFilePath().replace("/", "\\");

    qDebug() << "size" << sz;
    SwfSettings swfSettings(m_fileInfo.filePath(),
                            m_outFileName,
                            m_audioFileName,
                            sz,
                            parser->getColor(),
                            ProcessMethod::FFMPEG,
                            m_fps,
                            m_duration,
                            "mpeg4",
                            true,
                            m_fillWhite);

    worker->init(swfSettings);
    worker->run();

}

void Swf1080intro::on_finished(bool result)
{
    qDebug() << "on finished";
    emit changeLoop(true);
    doneTimer->start();
    setStateFinished();
    if (result) {
        ui->lbState->setText("Finished");
        emit introCreated();
    } else {
        ui->lbState->setText("Error!");
    }

}

void Swf1080intro::on_captureFinished()
{
    time2 = std::chrono::steady_clock::now();
    qDebug() << "Capture duration:" << std::chrono::duration_cast<std::chrono::milliseconds>(time2 - time1).count();
}

void Swf1080intro::setStateProcessing()
{
    ui->pbExport->setEnabled(false);
    ui->pbExport->setStyleSheet(
                "QPushButton { background: grey; color: white; }"
                "QPushButton:hover { border: 1px solid gray; }"
                );

    /* Blur effect for 'export' button */
    auto blurEffect = new QGraphicsBlurEffect(ui->pbExport);
    blurEffect->setBlurRadius(2);
    ui->pbExport->setGraphicsEffect(blurEffect);
    ui->lbState->setStyleSheet("QLabel { color: red; }");
    ui->lbState->setText("Process");

    /* Opacity effect to the 'state' label */
    auto opacityEffect = new QGraphicsOpacityEffect(ui->lbState);
    ui->lbState->setGraphicsEffect(opacityEffect);
    opacityAnimation = new QPropertyAnimation(opacityEffect, "opacity");
    opacityAnimation->setStartValue(1.0);
    opacityAnimation->setEndValue(0.1);
    opacityAnimation->setDuration(1000);
    opacityAnimation->setLoopCount(100);
    opacityAnimation->setEasingCurve(QEasingCurve::InOutSine);
    opacityAnimation->start();
}

void Swf1080intro::setStateFinished()
{
    ui->pbExport->setEnabled(true);
    ui->pbExport->setStyleSheet(
                "QPushButton { background: #335967; color: white; }"
                "QPushButton:hover { border: 1px solid gray; }"
                );
    ui->lbState->setStyleSheet("QLabel { color: yellow; }");
    ui->lbState->graphicsEffect()->setEnabled(false);
    ui->pbExport->graphicsEffect()->setEnabled(false);
    opacityAnimation->stop();
}

void Swf1080intro::setStateIdle()
{
    ui->lbState->setStyleSheet(
                "QLabel { color: white; }"
                );
    ui->lbState->setText("Idle");
}

void Swf1080intro::setStateError()
{
    ui->lbState->setStyleSheet(
                "QLabel { color: yellow; }"
                );
    ui->lbState->setText("Error!!!");
}


void Swf1080intro::on_tabClicked()
{
    emit clear();
    emit loadMovie(m_fileInfo);
}
