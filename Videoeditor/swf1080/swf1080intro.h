#ifndef Swf1080intro_H
#define Swf1080intro_H

#include "xmlparser.h"
#include "swfsettings.h"
#include "flashlistview.h"
#include "worker.h"
#include "imageprocessor.h"
#include "flashlistview.h"

#include <QWidget>
#include <QGridLayout>
#include <QStringListModel>
#include <QTimer>
#include <QDir>
#include <QDebug>
#include <QMap>
#include <QFileInfo>
#include <QFileDialog>
#include <QAxWidget>
#include <QModelIndex>
#include <QColorDialog>
#include <QPalette>
#include <QXmlStreamReader>
#include <QMap>
#include <QStringListModel>
#include <QModelIndex>
#include <QUrl>
#include <QTextStream>
#include <QApplication>
#include <QGraphicsOpacityEffect>
#include <QSettings>
#include <QMessageBox>

#include <memory>
#include <chrono>

namespace Ui {
class Swf1080intro;
}

class Swf1080intro : public QWidget
{
    Q_OBJECT

public:

    explicit Swf1080intro(QWidget *parent = 0);

    ~Swf1080intro();

public slots:

    void on_tabClicked();
    void setQuality(QVariant width,QVariant height);

private slots:

    void on_pbCancel_clicked();

    void on_pbColor_1_clicked();

    void on_cbSize_1_activated(const QString &arg1);

    void on_fontComboBox_1_activated(const QString &arg1);

    void on_leText_1_textChanged(const QString &arg1);

    void on_passFileInfo(const QFileInfo &info);

    void on_pbAddLogo_clicked();

    void on_pbAddAudio_clicked();

    void on_finished(bool result);

    void on_pbExport_clicked();

    void on_captureFinished();

private:

    Ui::Swf1080intro *ui;

    FlListView *m_lvFolders;

    Worker *worker;

    QSettings *m_settings;

    QTimer *doneTimer;

    QFileInfo m_fileInfo;

    QString m_audioFileName;

    XmlParser *parser;

    QStringListModel *textModel;

    QStringListModel *sizeModel_1;

    QStringListModel *exportModel;

    QModelIndex currentIndex;

    QColor currentColor_1 { "#000000" };

    int m_width;

    int m_height;

    QString m_root;

    uint m_duration;

    uint m_fps;

    QString m_xmlFileName;

    QString m_outFileName;

    bool m_fillWhite { true };

    QMap<QString, QSize> m_sizes;

    QStringList m_resolutions = QStringList() <<  "144" << "240" << "360" <<  "480" <<  "720" <<  "1080" ;


    QPropertyAnimation *opacityAnimation;

    /* time points for testing processing duration */
    std::chrono::steady_clock::time_point time1, time2;

    /* Private methods */

    bool readSettings();
    void setStyle();
    void setStateFinished();
    void setStateIdle();
    void setStateProcessing();
    void setStateError();

signals:

    void setFile(QString);
    void loadMovie(const QFileInfo &);
    void doRead();
    void changeText(uint, QString);
    void changeTextSize(uint i, QString);
    void changeTextColor(uint i, QString);
    void changeTextFont(uint i, QString);
    void changeLoop(bool);
    void getValues(uint);
    void clear();
    void release();
    void introCreated();

};

#endif // Swf1080intro_H
