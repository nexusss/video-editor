#include "flashlistmodel.h"

FlashListModel::FlashListModel(QString path, QObject *parent) :
    QAbstractListModel (parent),
    m_path {path}
{

}

QVariant FlashListModel::data(const QModelIndex &index, int role) const
{
    QDir dir (m_path);
    QStringList entries = dir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot);
    if ( role == Qt::DisplayRole ) {
        return entries.at(index.row());
    }
    return QVariant();
}

int FlashListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    QDir dir (m_path);
    dir.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot);
    return dir.count();
}

