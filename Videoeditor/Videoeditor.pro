#-------------------------------------------------
#
# Project created by QtCreator 2016-05-14T19:33:04
#
#-------------------------------------------------

QT       += core gui axcontainer xml network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += c++11
TARGET = Videoeditor
TEMPLATE = app

#include(../swflib.pri)
include(vlc-cut/vlc-cut.pri)
include(outro-maker/outro-maker.pri)
include(popUpWindow/popUpWindow.pri)

LIBS += -lgdi32
LIBS += -lUser32
macx|win32: LIBS += -L$$PWD/../x86/vc12/lib/ -lopencv_world300

INCLUDEPATH += $$PWD/../include
DEPENDPATH += $$PWD/../include

LIBS += -L$$PWD/../libs/ssllib/lib/ -lubsec -lws2_32 -llibeay32

INCLUDEPATH += $$PWD/../libs/ssllib/include
DEPENDPATH += $$PWD/../libs/ssllib/include


win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../x86/vc12/lib/opencv_world300.lib
else:macx|win32-g++: PRE_TARGETDEPS += $$PWD/../x86/vc12/lib/libopencv_world300.a

macx|win32: LIBS += -L$$PWD/../libs/swflib/ -lSwfLib

INCLUDEPATH += $$PWD/../libs/include
DEPENDPATH += $$PWD/../libs/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../libs/swflib/SwfLib.lib
else:macx|win32-g++: PRE_TARGETDEPS += $$PWD/../libs/swflib/libSwfLib.a

SOURCES += main.cpp\
        mainwindow.cpp \
    swf1080/previewwidget.cpp \
    swf1080/worker.cpp \
    swf1080/flashlistmodel.cpp \
    swf1080/flashlistview.cpp \
    swf1080/swf1080intro.cpp \
    swf1080/swf1080title.cpp \
    youtubeAction.cpp \
    filedownloader.cpp \
    projectdialog.cpp \
    projectsettings.cpp \
    mergefiles.cpp


HEADERS  += mainwindow.h \
    swf1080/previewwidget.h \
    swf1080/worker.h \
    swf1080/flashlistmodel.h \
    swf1080/flashlistview.h \
    swf1080/swf1080intro.h \
    swf1080/swf1080title.h \
    youtubeAction.h \
    filedownloader.h \
    projectdialog.h \
    projectsettings.h \
    mergefiles.h


FORMS    += mainwindow.ui \
    swf1080intro.ui \
    swf1080title.ui

RESOURCES += \
    resources.qrc \


win32:RC_ICONS += images/Icon.ico
