 #include "youtubeAction.h"
#include <QProcess>
#include <QtConcurrent/QtConcurrentRun>
#include <QRegExp>
#include <QFile>
#include <QDir>

struct MusicId{
    int id;
    QString ext;
};

YoutubeAction::YoutubeAction(QObject *parent):QObject(parent){

    fileDownloader = new FileDownloader(0);
    fileDownloader->moveToThread(&downLoaderThread);
    connect(&downLoaderThread,&QThread::finished,fileDownloader,&FileDownloader::deleteLater);
    connect(&downLoaderThread,&QThread::started,fileDownloader,&FileDownloader::init);
    connect(fileDownloader,&FileDownloader::downloaded,this,&YoutubeAction::downloadFinished);
    connect(fileDownloader,&FileDownloader::downloadedDASH,this,&YoutubeAction::mergeFiles);
    connect(fileDownloader,&FileDownloader::progress,this,&YoutubeAction::progress);
    downLoaderThread.start();
}


QString YoutubeAction::getYoutubeRealUrl(QString youtubeUrl){

    return getYoutubeRealUrl(youtubeUrl,"134/133");
}

QString YoutubeAction::getYoutubeRealUrl(QString youtubeUrl,QString quality){
    QString realUrl;
    QProcess process;
    QStringList strlist;

    strlist << "--no-check-certificate";
    strlist << "-f";
    strlist << quality;
    strlist << "-g";
    strlist << youtubeUrl;

    process.start(youtubeProgram, strlist);

    if( !process.waitForStarted() || !process.waitForFinished() ){
        emit realUrlWasFound("");
        return "";
    }

    realUrl = process.readAllStandardOutput();

    if(realUrl.isEmpty()){
        emit realUrlWasFound("");
        return "";
    }

    realUrl.replace("\r","");
    realUrl.replace("\n","");

    emit realUrlWasFound(realUrl);
    return realUrl;
}

void YoutubeAction::getYoutubeFileExt(QString youtubeUrl){

    QString fileExt;
    QProcess process;
    QStringList strlist;

    //get file extension
    strlist << "--no-check-certificate";
    strlist << "-f";
    strlist << "134/133";

    strlist << "--get-filename";
    strlist << youtubeUrl;
    process.start(youtubeProgram, strlist);

    if( !process.waitForStarted() || !process.waitForFinished() ){
        emit fileExtWasFound("");
        return ;
    }

    fileExt  = process.readAllStandardOutput();

    if(fileExt.isEmpty()){
        emit fileExtWasFound("");
        return ;
    }

    QStringList strList = fileExt.split(".");
    fileExt = "." + strList.last();
    fileExt.replace("\r","");
    fileExt.replace("\n","");

    emit fileExtWasFound(fileExt);
}

QVector <MusicId> searchMusicId(QStringList output){
    QVector <MusicId> mid;

    QRegExp musicReg("(\\d{2,3})(\\s+)([a-zA-Z0-9]{2,4})(\\s+)");

    foreach (QString str, output){
        if(str.contains(musicReg))
             if (str.contains("DASH") && str.contains("audio")){
                 qDebug() << "str" << str;
                 qDebug() << "music" << musicReg.indexIn(str);
                 if(musicReg.indexIn(str) >= 0){
                    MusicId tmp;
                    tmp.id = musicReg.cap(1).toInt();
                    tmp.ext = musicReg.cap(3);
                    mid.push_back(tmp);
                 }
             }
    }

    //delete
    QVector <MusicId> maxId;
    if(mid.size() > 0){
        maxId.push_back(mid.first());
        foreach (MusicId tmp, mid) {
            for(int i = 0; i < maxId.size(); i++){
                if(maxId.at(i).ext == tmp.ext){
                    if(maxId.at(i).id < tmp.id)
                        maxId[i].id = tmp.id;
                }
                else{
                    maxId.push_back(tmp);
                }
            }
        }
    }

    return maxId;
}

void YoutubeAction::getYoutubeQualityList(QString youtubeUrl){
    QStringList quality;
    QProcess process;
    QStringList strlist;

    qDebug() << "get youtube quality list";

    //get file extension
    strlist << "--no-check-certificate";
    strlist << "-F";

    strlist << youtubeUrl;
    process.start(youtubeProgram, strlist);

    if( !process.waitForStarted() || !process.waitForFinished() ){
        emit fileExtWasFound("");
        return ;
    }

    qDebug() << "proc end";

    QStringList output = QString(process.readAllStandardOutput()).split("\r\n");

    qDebug() << output;

    QRegExp reg("(\\d{2,3}\\s+)");
    QRegExp qualityReg("(\\d{2,3})(\\s+)([a-zA-Z0-9]{2,4})(\\s+)(\\d{3,4}x\\d{3,4})");
    QVector <MusicId> mid = searchMusicId( output);

    qDebug() << "misic ok";

    foreach (QString str, output) {
        if(str.contains(reg))
        {
//            if (str.contains("DASH") && !str.contains("audio")){
//                qDebug() << "not dash" << qualityReg.indexIn(str);
//                if (qualityReg.indexIn(str) >= 0){
//                    quality << qualityReg.cap(5).split("x")[1];
//                    Param prm;
//                    prm.id = qualityReg.cap(1);
//                    prm.ext = qualityReg.cap(3);
//                    prm.quality = qualityReg.cap(5).split("x")[1];
//                    params.push_back(prm);
//                    qDebug() << "1" << prm.id << prm.ext << prm.quality;
//                }
//            }

            if (str.contains("DASH") && !str.contains("audio")){
                qDebug() << "dash" << qualityReg.indexIn(str);
                if(qualityReg.indexIn(str) >= 0){
                    quality << qualityReg.cap(5).split("x")[1];
                    Param prm;
                    prm.id = qualityReg.cap(1);
                    prm.ext = qualityReg.cap(3);
                    prm.quality = qualityReg.cap(5).split("x")[1];
                        prm.musicExt = prm.ext == "mp4" ? "m4a" : "webm";
                        //search music id

                        foreach (MusicId tmp, mid) {
                            if(tmp.ext == prm.musicExt){
                                prm.musicId = QString::number(tmp.id);
                                break;
                            }
                        }
                        params.push_back(prm);
                }
            }

        }
    }

    quality.removeDuplicates();

    foreach (Param prm, params) {
        qDebug() << prm.id << prm.ext << prm.musicId << prm.musicExt << prm.quality;
    }
    qDebug() << "youtube action del duplicate" << quality;
    emit qualityListWasFound(quality);
}

void YoutubeAction::getRealUrl(QString youtubeUrl){

    QtConcurrent::run(this,&YoutubeAction::getYoutubeRealUrl,youtubeUrl);
}

void YoutubeAction::getFileExt(QString youtubeUrl){

    QtConcurrent::run(this,&YoutubeAction::getYoutubeFileExt,youtubeUrl);
}

void YoutubeAction::getQualityList(QString youtubeUrl){
    QtConcurrent::run(this,&YoutubeAction::getYoutubeQualityList,youtubeUrl);
}

Param searchParam(QVector <Param> params,QString quality){
    Param retParam;
    QVector <Param> tmpParam;
    //search quality
    foreach (Param prm, params) {
        qDebug() << "search prm" << prm.id << prm.ext << prm.quality;
        if(prm.quality == quality)
            tmpParam.push_back(prm);
    }
    if(tmpParam.size() == 0)
        return retParam;

    //search quality with mp4 extension
    foreach (Param prm, tmpParam) {
        qDebug() << "tmp param" << prm.id << prm.ext << prm.quality;
        if(prm.ext == "mp4"){
            retParam = prm;
        }
    }//if not found get first type
    if(retParam.id == ""){
        retParam = tmpParam.at(0);
    }
    qDebug() << "ret param" << retParam.id << retParam.ext << retParam.quality;
    return retParam;
}

void YoutubeAction::downloadYoutubeUrl(QString youtubeUrl,QString quality){

    Param param = searchParam(params,quality);
    if(param.id == 0)
        return;

    QString realUrl = getYoutubeRealUrl(youtubeUrl,param.id);

    qDebug() << "quality download" << quality << param.id << param.ext << param.quality;


    mDownloadedFileName = "";

//    if(quality == "1080"){
        QString downloadedFileName = QDir::tempPath() +  "/youtubeDownloadDASH" + "." + param.ext;
        QString mDownloadedFileName = QDir::tempPath() +  "/youtubeDownloadDASH" + "." + param.musicExt;

        QString mRealUrl = getYoutubeRealUrl(youtubeUrl,param.musicId);
        QMetaObject::invokeMethod(fileDownloader, "downloadUrlDASH", Qt::QueuedConnection,
                                   Q_ARG(QUrl, QUrl(realUrl)),
                                   Q_ARG(QUrl, QUrl(mRealUrl)),
                                   Q_ARG(QString, downloadedFileName),
                                   Q_ARG(QString, mDownloadedFileName));
//    }
//    else{
//        QString downloadedFileName = QDir::tempPath() +  "/youtubeDownload" + "." + param.ext;
//        QMetaObject::invokeMethod(fileDownloader, "downloadUrl", Qt::QueuedConnection,
//                               Q_ARG(QUrl, QUrl(realUrl)),
//                               Q_ARG(QString, downloadedFileName));

//    }
}

void YoutubeAction::downloadYoutube(QString youtubeUrl,QString quality){
    QtConcurrent::run(this,&YoutubeAction::downloadYoutubeUrl,youtubeUrl,quality);
}

void YoutubeAction::mergeYoutubeUrl(QString videoPath, QString audioPath){
    QProcess process;
    QStringList strlist;

    qDebug() << "start merge";
    QFileInfo fi(videoPath);
    //get file extension
    strlist << "-i";
    strlist << videoPath;
    strlist << "-i";
    strlist << audioPath;
    strlist << "-q:v"
            << "1"
            << "-qscale:v"
            << "1";
    strlist << "-c"
            << "copy";
    strlist << "-y";
    strlist << QDir::tempPath() +  "/youtubeDownload" + "." + fi.suffix();

    process.start(ffmpegProgram, strlist);

    if( !process.waitForStarted() || !process.waitForFinished() ){
        emit fileExtWasFound("");
        return ;
    }
    QFile logFile("download.txt");
    if(logFile.open(QIODevice::WriteOnly)){
        QTextStream out(&logFile);
        out << process.readAllStandardError();
        logFile.close();
    }
    emit downloadFinished(QDir::tempPath() +  "/youtubeDownload" + "." + fi.suffix());
    QFile::remove(videoPath);
    QFile::remove(audioPath);
}

void YoutubeAction::mergeFiles(QString videoPath, QString audioPath){
        QtConcurrent::run(this,&YoutubeAction::mergeYoutubeUrl,videoPath,audioPath);
}


YoutubeAction::~YoutubeAction(){
    QFile::remove(downloadedFileName);
    downLoaderThread.quit();
    downLoaderThread.wait();
}
