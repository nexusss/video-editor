import QtQuick 2.6
import QtQuick.Controls 1.5
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import Qt.labs.folderlistmodel 2.1
import CheckDir 1.0

Rectangle {
    id: rectangle4
    visible: true
    width: 420
    height: 221
    color: "#000000"
    signal exitClicked()
    signal projectNameChanged(string projectName)
    signal youtubeFile(string path)
    signal localFile(string path)
    signal projectFile(string path)
    signal screenRecording()
    property string projectPath: ""
    FileDialog{
        id: fileDialog
        title: "Open video"
        nameFilters: [ "Video files (*.mkv *.avi *.mp4 *.pl)" ]
        onAccepted: localFile(fileUrl.toString().replace("file:///",""))
    }

    FileDialog{
        id: projectFileDialog
        title: "Open project"
        nameFilters: [ "project file (*.vpp)" ]

        onAccepted: projectFile(fileUrl.toString().replace("file:///",""))
    }

    MessageDialog{
        id: messageDialog

        text: "Project already exist"
        onAccepted: {
        }
    }
    Checkdir{
        id: checkdir
    }

    RowLayout {
        id: rowLayout1
        x: 8
        y: 33
        width: 404
        height: 28

        Text {
            id: text1
            color: "#ffffff"
            text: qsTr("Project Name: ")
            font.pixelSize: 22
        }

        Rectangle {
            id: rectangle1
            color: "#ffffff"
            radius: 8
            width: 80
            height: 20
            Layout.fillHeight: true
            Layout.fillWidth: true
            TextInput {
                id: textInput1

                text: qsTr("New project")
                clip: true
                font.pixelSize: 22
                anchors.fill: parent
                onTextChanged: {
                    projectNameChanged(text)
                    if(text !== "" && (checkdir.checkDir(projectPath + "/" + text))){
                        messageDialog.open()
                    }
                }
                onCursorVisibleChanged: {
                    if(cursorVisible && text == "New project")
                        text = ""
                }
            }
        }
    }

    RowLayout {
        id: rowLayout2
        x: 8
        y: 62
        width: 404
        height: 28

        Text {
            id: text2
            color: "#ffffff"
            text: "Choose a video file:"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: true
            font.pixelSize: 22
        }
    }

    RowLayout {
        id: rowLayout4
        x: 8
        y: 190
        width: 404
        height: 28

        Text {
            color: "#ffffff"
            text: qsTr("or Load an existing Project:")
            verticalAlignment: Text.AlignVCenter
            Layout.fillWidth: true
            Layout.fillHeight: true
            horizontalAlignment: Text.AlignRight
            font.pixelSize: 16
        }

        Text {
            color: "#000000"
            text: qsTr("OPEN")
            verticalAlignment: Text.AlignVCenter
            Layout.fillHeight: true
            horizontalAlignment: Text.AlignHCenter
            Layout.minimumWidth: 60
            styleColor: "#ffffff"
            font.pixelSize: 16

            Rectangle {
                id: rectangle2
                color: "#ffffff"
                radius: 10
                z: -1
                anchors.fill: parent
            }

            MouseArea {
                id: mouseArea2
                anchors.fill: parent
                onClicked: {
                    console.log("projectFileDialog.folder",projectFileDialog.folder)
                    projectFileDialog.folder += "Projects"
                    projectFileDialog.open()
                }
            }
        }

    }

    Rectangle {
        id: rectangle5
        width: 420
        height: 27
        color: "#ffffff"

        RowLayout {
            id: rowLayout5
            anchors.rightMargin: 8
            anchors.leftMargin: 8
            anchors.bottomMargin: 0
            anchors.fill: parent

            Text {
                id: text3
                text: qsTr("Add or Load a Project")
                font.pixelSize: 19
            }

            Flickable {
                id: flickable1
                width: 300
                height: 300
                Layout.fillHeight: true
                Layout.fillWidth: true
                anchors.fill: parent
            }

            Text {
                id: text4
                text: qsTr("X")
                font.pixelSize: 25

                MouseArea {
                    id: mouseArea1
                    anchors.fill: parent
                    onClicked: exitClicked()
                }
            }
        }
    }

    RowLayout {
        id: rowLayout3
        x: 8
        y: 90
        width: 404
        height: 94

        MyButton {
            id: myButton1
            imgText: "Local File"
            imgSource: "icons/localfile.png"
            Layout.fillHeight: true
            Layout.fillWidth: true
            MouseArea{
                anchors.fill: parent
                onClicked: fileDialog.open()
            }
        }

        MyButton {
            id: myButton2
            imgText: "YouTube Video"
            imgSource: "icons/youtube.png"
            Layout.fillHeight: true
            Layout.fillWidth: true
            MouseArea{
                anchors.fill: parent
                onClicked: youtubeDownloader.visible = true
            }
        }

        MyButton {
            id: myButton3
            imgText:  "Screen Recording"
            imgSource: "icons/record.png"
            Layout.fillHeight: true
            Layout.fillWidth: true
            MouseArea{
                anchors.fill: parent
                onClicked: screenRecording()
            }
        }
    }

    YoutubeDownloader{
        id: youtubeDownloader
        width: 285
        height: 120
        anchors.verticalCenterOffset: 0
        anchors.horizontalCenterOffset: 8
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        visible: false
        onDownLoadFinished: {
            visible = false
            console.log("dowload fin",path)
            youtubeFile(path)
        }
    }

    Rectangle {
        id: rectangle3
        y: 185
        height: 2
        color: "#ffffff"
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
    }





}
