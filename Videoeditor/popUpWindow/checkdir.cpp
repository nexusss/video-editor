#include "checkdir.h"

CheckDir::CheckDir(QObject *parent) : QObject(parent)
{

}

bool CheckDir::checkDir(QString fullPath){
    QDir dir;
    dir.setPath(fullPath);
    return dir.exists();
}
