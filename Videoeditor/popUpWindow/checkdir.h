#ifndef CHECKDIR_H
#define CHECKDIR_H

#include <QObject>
#include <QDir>

class CheckDir : public QObject
{
    Q_OBJECT
public:
    explicit CheckDir(QObject *parent = 0);
    Q_INVOKABLE bool checkDir(QString fullPath);
signals:

public slots:
};

#endif // CHECKDIR_H
