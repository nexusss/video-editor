import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

import YoutubeAction 1.0

Rectangle{

    signal downLoadFinished(string path)
    width: 285
    height: 120

    Youtube{
        id: youtube
        onQualityListWasFound:{
            comboBoxModel.clear()
            for(var i = 0; i < qualityList.length; i++)
                comboBoxModel.append( {text: qualityList[i]})
            console.log(qualityList)

        }
        onProgress:{
            loadBar.value = value / maxValue
        }
        onDownloadFinished:  downLoadFinished(filePath)
    }

    ListModel{
        id: comboBoxModel
    }

    Text {
        id: text1
        x: 8
        y: 0
        text: qsTr("Youtube URL:")
        font.pixelSize: 22
    }

    TextField {
        id: urlInput
        x: 8
        y: 33
        width: 268
        height: 20
        text: qsTr("")
        font.pixelSize: 12
        onTextChanged: {
            comboBoxModel.clear()
            qualityComboBox.model = comboBoxModel

            if(!text)
                return true;

            var matches = text.match(/watch\?v=([a-zA-Z0-9\-_]+)/) ;
            var urlOk = (matches !== null)

                urlOk &= (text.indexOf("youtube") !== -1) || (text.indexOf("youtu.be") !== -1)

            if(urlOk)
                youtube.getQualityList(text)
        }
    }

    ProgressBar {
        id: loadBar
        x: 8
        y: 59
        width: 268
        height: 23
        minimumValue: 0
        onValueChanged: {
            percentText.text = ((value / maximumValue) * 100).toFixed(1)
            percentText.text += " %"
        }

        Text {
            id: percentText
            x: 103
            y: 5
            text: qsTr("")
            font.pixelSize: 12
        }
    }

    ComboBox {
        id: qualityComboBox
        x: 8
        y: 88
        model: comboBoxModel
    }

    Button {
        id: button1
        x: 201
        y: 87
        text: qsTr("Download")
        enabled: comboBoxModel.count
        onClicked: youtube.downloadYoutube(urlInput.text,qualityComboBox.currentText)
    }


    function checkYoutubeCorrection(url) {


    }
}
