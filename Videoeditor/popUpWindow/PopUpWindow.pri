
QT += qml quick widgets network

CONFIG += c++11

SOURCES += \
#    $$PWD/filedownloader.cpp
#    $$PWD/youtubeAction.cpp
    $$PWD/checkdir.cpp

RESOURCES += $$PWD/qml.qrc


# Default rules for deployment.
include(deployment.pri)

HEADERS += \
#    $$PWD/filedownloader.h
#    $$PWD/youtubeAction.h
    $$PWD/checkdir.h
