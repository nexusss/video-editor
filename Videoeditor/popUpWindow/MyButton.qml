import QtQuick 2.0
import QtQuick.Layouts 1.1

Item {
    property string imgSource: ""
    property string imgText: ""

    ColumnLayout {
        id: columnLayout1
        anchors.fill: parent

        Text {
            id: text1
            color: "#ffffff"
            text: imgText
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
            font.pixelSize: 16
        }

        Rectangle {
            id: rectangle1
            width: 200
            height: 200
            color: "#00000000"
            Layout.fillHeight: true
            Layout.fillWidth: true

            Image {
                id: image1
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.horizontalCenter: parent.horizontalCenter
                source: imgSource

                Layout.fillHeight: false
                Layout.fillWidth: false
            }
        }
    }
}
