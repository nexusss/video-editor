#include "projectdialog.h"
#include "popUpWindow/checkdir.h"
ProjectDialog::ProjectDialog(QString projectPath, QWidget *parent):QDialog(parent){
        qmlRegisterType<YoutubeAction>("YoutubeAction", 1, 0, "Youtube");
        qmlRegisterType<CheckDir>("CheckDir", 1, 0, "Checkdir");
        QHBoxLayout *mainLayout = new QHBoxLayout;
        popupView = new QQuickView();
        popupContainer = QWidget::createWindowContainer(popupView, this);
        mainLayout->addWidget(popupContainer);
        popupContainer->setMinimumSize(420, 221);
        popupContainer->setMaximumSize(420, 221);
        popupView->setSource(QUrl("qrc:/popup/main.qml"));
        popupView->rootObject()->setProperty("projectPath",projectPath);
        setLayout(mainLayout);
        setWindowFlags(Qt::FramelessWindowHint  | Qt::Dialog);
        connect(popupView->rootObject(),SIGNAL(localFile(QString)),this,SLOT(openLocalVideo(QString)));
        connect(popupView->rootObject(),SIGNAL(youtubeFile(QString)),this,SLOT(openYoutubeVideo(QString)));
        connect(popupView->rootObject(),SIGNAL(screenRecording()),this,SLOT(startScreenRecord()));
        connect(popupView->rootObject(),SIGNAL(projectNameChanged(QString)),this,SLOT(setProjectName(QString)));
        connect(popupView->rootObject(),SIGNAL(projectFile(QString)),this,SLOT(setProjectFile(QString)));
        connect(popupView->rootObject(),SIGNAL(localFile(QString)),this,SLOT(close()));
        connect(popupView->rootObject(),SIGNAL(youtubeFile(QString)),this,SLOT(close()));
        connect(popupView->rootObject(),SIGNAL(screenRecording()),this,SLOT(close()));
        connect(popupView->rootObject(),SIGNAL(projectFile(QString)),this,SLOT(close()));
        connect(popupView->rootObject(),SIGNAL(exitClicked()),qApp,SLOT(quit()));

}
ProjectType ProjectDialog::getProjectType(){
    return projectType;
}
QString ProjectDialog::getPath(){
    return videoPath;
}
QString ProjectDialog::getProjectName(){
    if(projectName.isEmpty()) return "New Project";
    else return projectName;
}

QString ProjectDialog::getProjectFile(){
    return projectFile;
}

void ProjectDialog::openLocalVideo(QString path){
    videoPath = path;
    projectType = ProjectType::LOCALVIDEO;
}

void ProjectDialog::openYoutubeVideo(QString path){
    videoPath = path;
    projectType = ProjectType::YOUTUBEVIDEO;
}

void ProjectDialog::startScreenRecord(){
    projectType = ProjectType::SCREENRECORDER;
}

void ProjectDialog::setProjectName(QString projectName){
    this->projectName = projectName;
}

void ProjectDialog::setProjectFile(QString projectFile){
    this->projectFile = projectFile;
    QFileInfo fi(projectFile);
    this->projectName = fi.baseName();
    projectType = ProjectType::LOADPROJECT;
}

void ProjectDialog::mousePressEvent(QMouseEvent *event){
        m_nMouseClick_X_Coordinate = event->x();
        m_nMouseClick_Y_Coordinate = event->y();
}

void ProjectDialog::mouseMoveEvent(QMouseEvent *event) {
        move(event->globalX()-m_nMouseClick_X_Coordinate,event->globalY()-m_nMouseClick_Y_Coordinate);
}
