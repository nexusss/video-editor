#ifndef PROJECTSETTINGS_H
#define PROJECTSETTINGS_H

#include <QObject>
#include <vlc-cut/vlc-cut.h>
#include <QFile>
#include <QXmlStreamWriter>
#include <vlc-cut/vlc-cut.h>
#include <outro-maker/outromaker.h>
#include "projectdialog.h"


class ProjectSettings : public QObject
{
    Q_OBJECT
    VlcCutMain *vlcCut;
    Outromaker *outroMaker;
    QFile file;
    ProjectType ptype;
public:
    explicit ProjectSettings(VlcCutMain *vlcCut,Outromaker *outroMaker, QObject *parent = 0);
    ~ProjectSettings();
    void setPtype(const ProjectType &ptype);
    ProjectType getPtype();
signals:

public slots:

    void setProjectName(QString projectPath, QString projectName);
    void save();
    void saveAs(QString fileName);
    void load(QString projectPath);

};

#endif // PROJECTSETTINGS_H
