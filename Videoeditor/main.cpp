#include "mainwindow.h"
#include <QApplication>
#include <../QmlVlc/QmlVlc.h>
#include <../QmlVlc/QmlVlcConfig.h>
#include <QQuickView>


int main(int argc, char *argv[])
{



    RegisterQmlVlc();
        QmlVlcConfig& config = QmlVlcConfig::instance();
        config.enableAdjustFilter( true );
        config.enableMarqueeFilter( true );
        config.enableLogoFilter( true );
        config.enableDebug( false );
    QApplication a(argc, argv);

    QStringList paths ;
    paths << QCoreApplication::applicationDirPath();

    QCoreApplication::setLibraryPaths(paths);


    MainWindow w;
    w.show();

    return a.exec();
}
