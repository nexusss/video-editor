#ifndef FILEDOWNLOADER_H
#define FILEDOWNLOADER_H

#include <QObject>
#include <QByteArray>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>

enum class DownloadType{VIDEO,VIDEO1080};

class FileDownloader : public QObject
{
 Q_OBJECT
    QFile downloadFile;//for video
    QFile mdownloadFile;//for audio
    DownloadType downloadType;

 public:
  explicit FileDownloader( QObject *parent = 0);

  virtual ~FileDownloader();

 signals:
    void downloaded(QString fileName);
    void downloadedDASH(QString fileName,QString audioFileName);
    void progress(qint64 bytesReceived, qint64 bytesTotal);
 private slots:
    void fileDownloaded(QNetworkReply* pReply);
    void mfileDownloaded(QNetworkReply* pReply);
    void saveFile();
    void savemFile();

public slots:
  void downloadUrl(QUrl url,QString fileName);
  void downloadUrlDASH(QUrl vurl,QUrl murl,QString vFileName,QString mFileName);
  void init();

 private:
  QNetworkAccessManager *WebCtrl;
  QNetworkAccessManager *m_WebCtrl;
  QNetworkReply* pReply;
  QNetworkReply* mpReply;
};

#endif // FILEDOWNLOADER_H
