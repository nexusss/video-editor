#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDir>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QDir dir;
    defaultProjectPath = dir.absolutePath() + "/Projects";
    ui->setupUi(this);

    QDir projectDir;
    projectDir.mkdir("Projects");
    pd = new ProjectDialog(defaultProjectPath,this);

    mf = new MergeFiles(this);
    connect(mf,&MergeFiles::mergeFinished,this,&MainWindow::mergeFin);
    connect(pd,SIGNAL(finished(int)),this,SLOT(dialogFinished()));

    ui->recordButton->setIcon(QIcon(":/images/record.png"));
    ui->recordButton->setIconSize(ui->recordButton->size());
    ui->recordButton->setFlat(true);


    //#74E6AA
    QString bottomLabelStyleSheet = "QLabel { background-color : gray; color: white; border-radius: 10px;}";
    ui->titleLabel->setStyleSheet(bottomLabelStyleSheet);
    ui->introLabel->setStyleSheet(bottomLabelStyleSheet);
    ui->mvLabel->setStyleSheet(bottomLabelStyleSheet);
    ui->outroLabel->setStyleSheet(bottomLabelStyleSheet);

    m_swfControlsIntro = new Swf1080intro(ui->tabIntro);
    m_swfControlsTitle = new Swf1080title(ui->tabTitle);

    m_axWidget = new PreviewWidget(ui->frame_1);
    m_axWidget->resize(ui->frame_1->width(), ui->frame_1->height());


    connect(m_swfControlsIntro, &Swf1080intro::loadMovie, m_axWidget, &PreviewWidget::on_loadMovie);
    connect(m_swfControlsIntro, &Swf1080intro::clear, m_axWidget, &PreviewWidget::on_clear);
    connect(m_swfControlsTitle, &Swf1080title::loadMovie, m_axWidget, &PreviewWidget::on_loadMovie);
    connect(m_swfControlsTitle, &Swf1080title::clear, m_axWidget, &PreviewWidget::on_clear);

    setStyle();

    m_swfControlsIntro->on_tabClicked();//loading flash player before qml gui, because if it load after qml, qml disappear
        vlccutView = new QQuickView();
                vlccutContainer = QWidget::createWindowContainer(vlccutView, ui->frame_2);
                vlccutContainer->setMinimumSize(420, 541);
                vlccutContainer->setMaximumSize(420, 541);
            vlccutView->setSource(QUrl("qrc:/vlccut/main.qml"));

            vlccut = new VlcCutMain(vlccutView);
            vlccut->moveToThread(&vlccutThread);

    vlccutMainPage = vlccutView->rootObject()->findChild<QObject*>("mainPageName");
    connect(&vlccutThread,&QThread::started,vlccut,&VlcCutMain::init);
    connect(&vlccutThread,&QThread::finished,vlccut,&VlcCutMain::deleteLater);
    vlccutThread.start();


            qmlRegisterType<VideoPlayerParam>("VideoPlayerParam", 1, 0, "VideoParam");
            qmlRegisterType<YoutubeAction>("YoutubeAction", 1, 0, "Youtubeaction");
            outroMakerView = new QQuickView();
            outroMakerContainer = QWidget::createWindowContainer(outroMakerView, ui->frame_3);
                    outroMakerContainer->setMinimumSize(420, 541);
                    outroMakerContainer->setMaximumSize(420, 541);
                    outroMakerContainer->setFocusPolicy(Qt::TabFocus);
                outroMakerView->setSource(QUrl("qrc:/outromakerqml/main.qml"));

                QObject *outroSettingsPage = outroMakerView->rootObject()->findChild<QObject*>("settingsTab");

                outromaker = new Outromaker(outroMakerView,this);

    connect(outromaker,&Outromaker::recordIsOver,this,&MainWindow::outroVideoActivate);
    connect(outromaker,&Outromaker::recordIsOver,mf,&MergeFiles::setOutroPath);
    connect(vlccut,&VlcCutMain::videoAdded,this,&MainWindow::vlccutVideoActivate);
    connect(vlccut,&VlcCutMain::videoAdded,mf,&MergeFiles::setMainPath);
    connect(m_swfControlsIntro, &Swf1080intro::introCreated,this,&MainWindow::introActivate);
    connect(m_swfControlsTitle, &Swf1080title::titleCreated,this,&MainWindow::titleActivate);

    connect(vlccutMainPage,SIGNAL(sendVideoSize(QVariant,QVariant)),m_swfControlsIntro,SLOT(setQuality(QVariant,QVariant)));
    connect(vlccutMainPage,SIGNAL(sendVideoSize(QVariant,QVariant)),m_swfControlsTitle,SLOT(setQuality(QVariant,QVariant)));
    connect(vlccutMainPage,SIGNAL(sendVideoSize(QVariant,QVariant)),outroSettingsPage,SLOT(setQuality(QVariant,QVariant)));
    connect(vlccutMainPage,SIGNAL(sendVideoSize(QVariant,QVariant)),this,SLOT(setSize(QVariant,QVariant)));
    connect(vlccutMainPage,SIGNAL(sendVideoName(QString)),this,SLOT(setVideoName(QString)));

    ps = new ProjectSettings(vlccut,outromaker,this);
    pd->show();
    pd->setModal(true);

    pd->move(((this->width() / 2) - (420 / 2)),(this->height() / 2) +  (200 / 2));
}

void MainWindow::mergeFin(){
    QMessageBox msgBox;
     msgBox.setText("Record finished");
     msgBox.exec();

}

MainWindow::~MainWindow()
{
    vlccutThread.quit();
    outromakerThread.quit();
    vlccutThread.wait();
    outromakerThread.wait();
    delete ui;
}

void MainWindow::loadNewProject(QString projectName, bool newProject){


    QDir projectDir;
    int i = 0;
    QString tmpProjectName = projectName;
    while(projectDir.exists(defaultProjectPath + "/" + tmpProjectName) && newProject){
        i++;
        tmpProjectName = projectName;
        tmpProjectName += " (" + QString::number(i) +")";
    }

    projectName = tmpProjectName;

    projectDir.mkdir("Projects/" + projectName);

    setWindowTitle("Video Producer PRO v1.0 - " + projectName);
    ui->projectNameLabel->setText(projectName);

    projectDir.mkdir(defaultProjectPath + "/" + projectName);
    outromaker->setProjectPath(projectDir.absolutePath() + "/Projects/" + projectName);
    ps->setProjectName(projectDir.absolutePath() + "/Projects/" + projectName,projectName + ".vpp");
    outroVideoDisactivate();
    vlccutVideoDisactivate();
    introDisactivate();
    titleDisactivate();
    mf->setTitlePath("");
    mf->setIntroPath("");
    mf->setMainPath("");
    mf->setOutroPath("");
}

void MainWindow::checkUpButton(ProjectType pType){
    switch (pType) {
    case ProjectType::LOCALVIDEO:{
            ui->localFileButton->setChecked(true);
            ui->youtubeButton->setChecked(false);
            ui->screenRecording->setChecked(false);
            break;
        }
    case ProjectType::YOUTUBEVIDEO:{
            ui->localFileButton->setChecked(false);
            ui->youtubeButton->setChecked(true);
            ui->screenRecording->setChecked(false);
            break;
        }
    case ProjectType::SCREENRECORDER:{
            ui->localFileButton->setChecked(false);
            ui->youtubeButton->setChecked(false);
            ui->screenRecording->setChecked(true);
            break;
        }
    }
}

void MainWindow::dialogFinished(){


    if(pd->getProjectType() == ProjectType::LOADPROJECT){
        loadNewProject(pd->getProjectName(),false);
        checkUpButton(ps->getPtype());
        ps->load(pd->getProjectFile());
    }
    else{
        loadNewProject(pd->getProjectName(),true);
        checkUpButton(pd->getProjectType());
        switch (pd->getProjectType()) {
        case ProjectType::LOCALVIDEO:
        case ProjectType::YOUTUBEVIDEO:{
                vlccutMainPage->setProperty("videoFilePath","");
                vlccutMainPage->setProperty("videoFilePath",pd->getPath());
            }
        case ProjectType::SCREENRECORDER:{
                ps->setPtype(pd->getProjectType());
                break;
            }
        }
    }
}

void MainWindow::setStyle()
{
    QString style {
        "MainWindow { background: #000000; } "
        "QFrame { background: #222222; } "
        "QPushButton { background-color: #333435; color: white; }"
        "QPushButton:hover { border: 1px solid gray; }"
        "QTreeView { background: #34383b; color: white; } "
        "QTreeView::item:selected { background-color: #8fbdf5; color: white; }"
        "QScrollBar { background: #1A2430; } "
        "QLineEdit { background: #222222; border: 1px solid gray; color: white; } "
        "QLineEdit:hover { border: 1px solid white; background-color: #5b4a3f; }"
        "QComboBox { background: #222222; color: white; } "
        "QComboBox:hover { border: 1px solid white; } "
        "QListView { background: #222222; color: white; } "
        "QListView:hover { border: 1px solid white; } "
        "QListView::item:selected { background-color: #D6DFF2; color: white; }"
        "QListView::item:hover { background-color: #EC0402; color: white; }"
        "QCheckBox { color: white; }"
        "QGroupBox { color: white; }"
        "QLabel { color: white; }"
        "QToolBar {background: #222222;spacing: 3px;color: white; }"
    };

    QString buttonstyle = "QPushButton{"
                         "border: 2px;"
                         "background-color: grey;"
                         "border-color: grey;"
                         "border-radius: 10px;"
                         "color: white;}"
                         "QPushButton:checked{"
                         "border: 2px;"
                         "background-color: #74E6AA;"
                         "border-color: #74E6AA;"
                         "border-radius: 10px;"
                         "color: black;}";

    ui->localFileButton->setStyleSheet(buttonstyle);
    ui->youtubeButton->setStyleSheet(buttonstyle);
    ui->screenRecording->setStyleSheet(buttonstyle);
    this->setStyleSheet(style);
}

void MainWindow::on_exitButton_clicked(){
    qApp->quit();
}

void MainWindow::on_saveButton_clicked(){
    ps->save();
    QMessageBox msgBox;
     msgBox.setText("Project saved.");
     msgBox.exec();
}

void MainWindow::on_saveAsButton_clicked(){
    QString fileName = QFileDialog::getSaveFileName(this,
          tr("Save project"), "Projects/" + ui->projectNameLabel->text(), tr("Project File (*.vpp)"));
    if(!fileName.isEmpty()){
    ps->saveAs(fileName);
        QMessageBox msgBox;
        msgBox.setText("Project saved.");
        msgBox.exec();
    }
}

void MainWindow::on_loadButton_clicked(){
    QString fileName = QFileDialog::getOpenFileName(this,
          tr("Save project"), "Projects/" + ui->projectNameLabel->text(), tr("Project File (*.vpp)"));
    if(!fileName.isEmpty()){
        QFileInfo fi(fileName);
        loadNewProject(fi.baseName(),false);
        ps->load(fileName);
    }
}

void MainWindow::onUpButtonClicked(QPushButton *buttonClicked,QPushButton *firstButton, QPushButton *secondButton){
//    if(firstButton->isChecked() || secondButton->isChecked()){
        QMessageBox msgBox;
         msgBox.setText("SAVE and LEAVE the actual project");
         msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
         msgBox.setDefaultButton(QMessageBox::Save);
         int ret = msgBox.exec();
         switch (ret){
         case QMessageBox::Save:{
                 on_saveButton_clicked();
                 firstButton->setChecked(false);
                 secondButton->setChecked(false);
                 pd->show();
                 break;
         }
         case QMessageBox::Cancel:{
                buttonClicked->setChecked(false);
                 break;
         }
         }
}

void MainWindow::on_localFileButton_clicked(){
    onUpButtonClicked(ui->localFileButton,ui->youtubeButton,ui->screenRecording);
}

void MainWindow::on_youtubeButton_clicked(){
    onUpButtonClicked(ui->youtubeButton,ui->localFileButton,ui->screenRecording);
}

void MainWindow::on_screenRecording_clicked(){
    onUpButtonClicked(ui->screenRecording,ui->localFileButton,ui->youtubeButton);
}

void MainWindow::on_recordButton_clicked(){
    mf->merge();
}

void MainWindow::setSize(QVariant width,QVariant height){
    ui->sizeLabel->setText(width.toString() + "x" + height.toString());
}

void MainWindow::setVideoName(QString fileName){
    ui->videoNameLabel->setText(fileName);
}

void MainWindow::outroVideoActivate(){
    QString bottomLabelStyleSheet = "QLabel { background-color : #74E6AA; color: white; border-radius: 10px;}";
    ui->outroLabel->setStyleSheet(bottomLabelStyleSheet);
}

void MainWindow::vlccutVideoActivate(){
    QString bottomLabelStyleSheet = "QLabel { background-color : #74E6AA; color: white; border-radius: 10px;}";
    ui->mvLabel->setStyleSheet(bottomLabelStyleSheet);
}

void MainWindow::introActivate(){
    QString bottomLabelStyleSheet = "QLabel { background-color : #74E6AA; color: white; border-radius: 10px;}";
    ui->introLabel->setStyleSheet(bottomLabelStyleSheet);
}

void MainWindow::titleActivate(){
    QString bottomLabelStyleSheet = "QLabel { background-color : #74E6AA; color: white; border-radius: 10px;}";
    ui->titleLabel->setStyleSheet(bottomLabelStyleSheet);
}

void MainWindow::outroVideoDisactivate(){
    QString bottomLabelStyleSheet = "QLabel { background-color : gray; color: white; border-radius: 10px;}";
    ui->outroLabel->setStyleSheet(bottomLabelStyleSheet);
}

void MainWindow::vlccutVideoDisactivate(){
    QString bottomLabelStyleSheet = "QLabel { background-color : gray; color: white; border-radius: 10px;}";
    ui->mvLabel->setStyleSheet(bottomLabelStyleSheet);
}

void MainWindow::introDisactivate(){
    QString bottomLabelStyleSheet = "QLabel { background-color : gray; color: white; border-radius: 10px;}";
    ui->introLabel->setStyleSheet(bottomLabelStyleSheet);
}

void MainWindow::titleDisactivate(){
    QString bottomLabelStyleSheet = "QLabel { background-color : gray; color: white; border-radius: 10px;}";
    ui->titleLabel->setStyleSheet(bottomLabelStyleSheet);
}



void MainWindow::on_twIntroTitle_tabBarClicked(int index)
{
    if (index == 0) {
        m_swfControlsIntro->on_tabClicked();
    }
    if (index == 1) {
        m_swfControlsTitle->on_tabClicked();
    }
}
