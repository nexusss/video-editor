#include "mergefiles.h"
#include <QProcess>
#include <QtConcurrent/QtConcurrent>
#include <QTextStream>
#include <QDebug>
MergeFiles::MergeFiles(QObject *parent) : QObject(parent)
{
    mergeFile.setFileName("merge.txt");

}

void MergeFiles::setIntroPath(QString path){
    introPath = path;
}

void MergeFiles::setTitlePath(QString path){
    titlePath = path;
}

void MergeFiles::setMainPath(QString path){
    mainVideo = path;
}

void MergeFiles::setOutroPath(QString path){
    outroPath = path;
}

void MergeFiles::ffmpegProc(QStringList param){
    QProcess *process = new QProcess;

    process->start("ffmpeg.exe ",param);


    process->waitForFinished();
    qDebug() << process->readAllStandardError();
    emit mergeFinished();
    mergeFile.remove();
    delete process;
}

void MergeFiles::merge(){
    QStringList params;

    QDir finalDir;
    finalDir.mkdir("Final-Output");
    if(mergeFile.open(QIODevice::WriteOnly)){
        QTextStream out(&mergeFile);

        if(!introPath.isEmpty())
            out << "file" << "\t" << "'" + introPath + "'\n" ;
        if(!titlePath.isEmpty())
            out << "file" << "\t" << "'" + titlePath + "'\n";
        if(!mainVideo.isEmpty())
            out << "file" << "\t" << "'" + mainVideo + "'\n";
        if(!outroPath.isEmpty())
            out << "file" << "\t" << "'" + outroPath + "'\n";
        mergeFile.close();
    }

    params << "-f"
           << "concat"
           << "-i"
           << mergeFile.fileName()
           << "-c"
           << "copy"
           << "-y"
           << finalDir.absolutePath() + "/Final-Output/" + "out.mp4";
    QtConcurrent::run(this,&MergeFiles::ffmpegProc,params);
}
