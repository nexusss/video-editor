#ifndef SWFSETTINGS_H
#define SWFSETTINGS_H

#include "swflib_global.h"
#include <QtCore>
#include <QColor>

enum class ProcessMethod;

#include "imageprocessor.h"

struct SWFLIBSHARED_EXPORT SwfSettings
{

    SwfSettings() {;}

    SwfSettings::SwfSettings(QString path,
                             QString outFile,
                             QString audioFile,
                             QSize outSize,
                             QColor transparentColor,
                             ProcessMethod method,
                             uint fps = 15,
                             uint duration = 6000,
                             QString videoCodec = "mpeg4",
                             bool Compressed = true,
                             bool FillWhite = true);

    QString WorkingFolder { "" };

    QString OutFile { "temp.mov" };

    uint FrameRate { 25 };

    uint Duration { 6 };

    QString VideoCodec { "mpeg4" };

    ProcessMethod Method;

    QColor Color { QColor("#000000") };

    QSize Size { QSize(0, 0) };

    QString AudioFile { "" };

    bool Compressed { true };

    bool FillWhite { false };

};


#endif // SWFSETTINGS_H
