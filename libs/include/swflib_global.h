#ifndef SWFLIB_GLOBAL_H
#define SWFLIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(SWFLIB_LIBRARY)
#  define SWFLIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define SWFLIBSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // SWFLIB_GLOBAL_H
