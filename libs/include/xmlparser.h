#ifndef XMLPARSER_H
#define XMLPARSER_H

#include "swflib_global.h"
#include <QObject>
#include <QMap>
#include <QFile>
#include <QDebug>
#include <QMap>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDomDocument>
#include <QTextStream>
#include <QSize>
#include <QColor>

class SWFLIBSHARED_EXPORT XmlParser : public QObject
{

    Q_OBJECT

public:

    explicit XmlParser(QObject *parent = 0);

    bool valid() const { return m_valid; }

    QSize getSize() const;

    QColor getColor() const;

    /* Return duration in milliseconds */
    uint getDuration() const;

signals:

    void displayText(uint, QString);

    void displayColor(uint, QString);

    void displayFont(uint, QString);

    void displaySize(uint, QString);

public slots:

    /* Set xml file
     * fileName:        xml file name
     */
    void on_setFile(QString fileName);

    /* Read xml file contents */
    void read();

    /* Change text
     * text:            text itself
     * i:               text number
     */
    void on_changeText(uint i, QString text);

    /* Change text font
     * sizeString:      font size
     * i:               text number
     */
    void on_changeTextSize(uint i, QString sizeString);

    /* Change text color
     * colorString:     in format "0xF7A030"
     * i:               text number
     */
    void on_changeTextColor(uint i, QString colorString);

    /* Change background color
     * colorString:     in format "0xF7A030"
     * i:               text number
     */
    void on_changeBkgColor(QString colorString);

    /* Change text font
     * fontString:      name of font in the system (given automatically by QFontComboBox
     * i:               text number
     */
    void on_changeTextFont(uint i, QString fontString);

    /* Enable / disable rewind */
    void on_changeLoop(bool);

    /* Get values
     * i:               text number
     */
    void on_getValues(uint i);

    QString getValue(QString fileName, QString key) const;

    bool setValue(QString fileName, QString key, QString value);

private:

    QString m_fileName;

    bool m_valid;

    void displayValues(uint textNumber);

    QStringList m_keys  = (QStringList() <<
                "text1" <<
                "text1_color" <<
                "text1_family" <<
                "text1_size" <<
                "text2" <<
                "text2_color" <<
                "text2_family" <<
                "text2_size" <<
                "duration" <<
                "dimensions" <<
                "transparent_color"
                          );
    QDomDocument m_document;

    void write();

};

#endif // XMLPARSER_H
