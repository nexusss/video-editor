#include "xmlparser.h"

XmlParser::XmlParser(QObject *parent) : QObject(parent), m_document("xmldocument")
{

}


void XmlParser::read()
{
    QFile xmlFile (m_fileName);

    if (!xmlFile.open(QFile::ReadOnly)) {
        qDebug() << "Can't open xml file";
        xmlFile.close();
        m_valid = false;
        return;
    }

    if (!m_document.setContent(&xmlFile)) {
        qDebug() << "Can't read xml document";
        xmlFile.close();
        m_valid = false;
        return;
    }

    m_valid = true;

    displayValues(1);
    displayValues(2);

    xmlFile.close();

}

QString XmlParser::getValue(QString fileName, QString key) const
{
    QFile xmlFile (fileName);
    if (!xmlFile.open(QFile::ReadOnly)) {
        qDebug() << "Can't open xml file";
        xmlFile.close();
        return QString();
    }

    QDomDocument document;
    if (!document.setContent(&xmlFile)) {
        qDebug() << "Can't read xml document";
        xmlFile.close();
        return QString();
    }

    return document.elementsByTagName(key).at(0).toElement().text();

}


bool XmlParser::setValue(QString fileName, QString key, QString value)
{
    QFile xmlFile (fileName);
    if (!xmlFile.open(QFile::ReadOnly)) {
        qDebug() << "Can't open xml file for reading: " << fileName;
        xmlFile.close();
        return false;
    }

    QDomDocument document;
    if (!document.setContent(&xmlFile)) {
        qDebug() << "Can't read xml document";
        xmlFile.close();
        return false;
    }

    xmlFile.close();

    if (!xmlFile.open(QFile::ReadWrite | QFile::Truncate | QFile::Text)) {
        qDebug() << "Can't open xml file for writing: "  << fileName;
        xmlFile.close();
        return false;
    }

    auto element = document.elementsByTagName(key).at(0).toElement();
    if (element.isNull()) {
        qDebug() << "Element not found";
        return false;
    }
    element.firstChild().setNodeValue(value);
    QTextStream stream;
    stream.setDevice(&xmlFile);
    document.save(stream, QDomNode::EncodingFromDocument);
    xmlFile.close();

    return true;
}

QSize XmlParser::getSize() const
{

    auto sizeString = m_document.elementsByTagName("dimensions").at(0).toElement().text();

    if (sizeString == "") {
        return QSize();
    }

    auto sizeList = sizeString.split("x");

    auto width = sizeList.at(0).toInt();
    auto height = sizeList.at(1).toInt();

    return QSize(width, height);

}


QColor XmlParser::getColor() const
{
    auto colorString = m_document.elementsByTagName("transparent_color").at(0).toElement().text();

    QColor color(colorString.replace("0x", "#", Qt::CaseInsensitive));

    return color;

}


uint XmlParser::getDuration() const
{
    return m_document.elementsByTagName("duration").at(0).toElement().text().toInt();
}


void XmlParser::on_setFile(QString inFileName)
{

    m_fileName = inFileName;

}


void XmlParser::on_changeText(uint i, QString str)
{

    auto textElement = m_document.elementsByTagName("text" + QString::number(i)).at(0);
    if (textElement.isNull()) {
        return;
    }

    textElement.firstChild().setNodeValue(str);
    write();

}


void XmlParser::on_changeTextSize(uint i, QString str)
{

    auto textElement = m_document.elementsByTagName("text" + QString::number(i) + "_size").at(0);
    if (textElement.isNull()) {
        return;
    }

    textElement.firstChild().setNodeValue(str);
    write();

}


void XmlParser::on_changeTextColor(uint i, QString str)
{

    auto textElement = m_document.elementsByTagName("text" + QString::number(i) + "_color").at(0);
    if (textElement.isNull()) {
        return;

    }

    textElement.firstChild().setNodeValue(str.replace("#", "0x"));
    write();
}


void XmlParser::on_changeBkgColor(QString str)
{

    auto textElement = m_document.elementsByTagName("stage_color").at(0);
    if (textElement.isNull()) {
        return;

    }

    textElement.firstChild().setNodeValue(str.replace("#", "0x"));
    write();
}


void XmlParser::on_changeTextFont(uint i, QString str)
{
    qDebug() << "Changing font family";

    auto textElement = m_document.elementsByTagName("text" + QString::number(i) + "_family").at(0);
    if (textElement.isNull()) {
        return;
    }

    textElement.firstChild().setNodeValue(str);

    write();

}


void XmlParser::on_changeLoop(bool loopEnabled)
{
    auto loopElement = m_document.elementsByTagName("loop").at(0);
    if (loopElement.isNull()) {
        return;
    }
    if (loopEnabled) {
        loopElement.firstChild().setNodeValue("1");
    } else {
        loopElement.firstChild().setNodeValue("0");
    }
    write();
    return;
}


void XmlParser::on_getValues(uint i)
{
    emit displayColor(i + 1, m_document.elementsByTagName("text" + QString::number(i + 1) + "_color").at(0).toElement().text());

    emit displayFont(i + 1, m_document.elementsByTagName("text" + QString::number(i + 1) + "_family").at(0).toElement().text());

    emit displaySize(i + 1, m_document.elementsByTagName("text" + QString::number(i + 1) + "_size").at(0).toElement().text());
}


void XmlParser::displayValues(uint textNumber)
{

    /* Set values */

    emit displayText(textNumber, m_document.elementsByTagName("text" + QString::number(textNumber)).at(0).toElement().text());

    emit displayColor(textNumber, m_document.elementsByTagName("text" + QString::number(textNumber) + "_color").at(0).toElement().text());

    emit displayFont(textNumber, m_document.elementsByTagName("text" + QString::number(textNumber) + "_family").at(0).toElement().text());

    emit displaySize(textNumber, m_document.elementsByTagName("text" + QString::number(textNumber) + "_size").at(0).toElement().text());

}


void XmlParser::write()
{
    /* Write the result back to the file */

    QFile xmlFile (m_fileName);

    /* Delete file contents if any */

    xmlFile.open(QFile::ReadWrite | QFile::Truncate | QFile::Text);

    QTextStream stream;

    stream.setDevice(&xmlFile);

    m_document.save(stream, QDomNode::EncodingFromDocument);

    xmlFile.close();
}


