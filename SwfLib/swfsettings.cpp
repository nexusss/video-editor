#include "swfsettings.h"

SwfSettings::SwfSettings(QString path,
                         QString outFile,
                         QString audioFile,
                         QSize outSize,
                         QColor transparentColor,
                         ProcessMethod method,
                         uint fps,
                         uint duration,
                         QString videoCodec,
                         bool Compressed,
                         bool FillWhite):

    WorkingFolder(path),
    OutFile(outFile),
    AudioFile(audioFile),
    Size(outSize),
    Color(transparentColor),
    Method(method),
    FrameRate(fps),
    Duration(duration),
    VideoCodec(videoCodec),
    Compressed(Compressed),
    FillWhite(FillWhite)

{

}
