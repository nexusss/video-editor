#ifndef IMAGEPROCESSOR_H
#define IMAGEPROCESSOR_H

#include "swflib_global.h"
#include "swfsettings.h"
#include <QObject>
#include <QFile>
#include <QSize>
#include <QImage>
#include <QDebug>
#include <QDateTime>
#include <QDir>
#include <QProcess>
#include <QApplication>
#include <QTextStream>
#include <QDateTime>
#include <QtConcurrent/QtConcurrent>

#include <memory>
#include <chrono>
#include <thread>


#include <opencv2/opencv.hpp>

struct SwfSettings;


enum class ProcessMethod { FFMPEG, OPENCV } ;

class SWFLIBSHARED_EXPORT ImageProcessor : public QObject
{

    Q_OBJECT

public:

    explicit ImageProcessor(QObject *parent = 0);

    /* Input: QImage
     *
     * Converts QImage to cv::Mat and saves to m_buffer
     */
    void capture(QImage);

    /* Saves images accumulated in m_buffer to a video file
     * with parameters specified when calling ImageProcessor::initialize()
     */
    void save();



public slots:

    /* Intitializes ImageProcessor with SwfSettings structure
     */
    void initialize(SwfSettings);

    /* Initializes ImageProcessor with parameters
     *
     * @param
     * path:        absolute path to working folder
     * outFile:     absolute path to output video file
     * audioFile:   absolute path to audio file to combine with output video file
     * outSize:     output video frame size
     * transparentColor:    color to define as transparent
     * method:      processing method, FFMPEG by default
     * fps:         frame rate (frames per second)
     * duration:    clip duration, ms
     * videoCodec:  video codec
     * compressed:  save output video compressed or uncompressed
     * fillwhite:   fill 'transparent' color with white
     *
     * @return:     void
     */
    void initialize(QString path,
                    QString outFile,
                    QString audioFile,
                    QSize outSize,
                    QColor transparentColor,
                    ProcessMethod method = ProcessMethod::FFMPEG,
                    uint fps = 15,
                    uint duration = 6000,
                    QString videoCodec = "mpeg4",
                    bool Compressed = true,
                    bool FillWhite = true);

    void on_Release();


private:

    static int m_counter;

    ProcessMethod m_method;

    std::shared_ptr<cv::VideoWriter> m_writer;

    cv::Size m_size;

    cv::Mat toTransparent(cv::Mat);

    QString m_path;

    QString m_outFile;

    QString m_audioFile { "" };

    QProcess *m_process { nullptr };

    uint m_fps;

    uint m_duration;

    QString m_videoCodec;

    bool m_compressed { true };

    QStringList ticks;

    std::vector<std::vector<uchar>> m_buffer;

    QColor m_transparentColor;

    bool m_fillWhite;

    void saveToBuffer(QImage);

    void processBuffer();

    void processImage(cv::Mat, uint);

    /* To another thread */

    QStringList ffmpegArgs();

private slots:

        void encode();

signals:

    /* FFMPEG error string if Exit code != 1, otherwise empty string */
    void ffmpegFinished(QString);

    /* Start FFMPEG encoding */
    void start();

    /* Emit finished(true) if completed OK, emit finished(false) if failed */
    void finished(bool);


};

#endif // IMAGEPROCESSOR_H
