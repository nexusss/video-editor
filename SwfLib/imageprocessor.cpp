#include "imageprocessor.h"

using namespace std;
using namespace cv;

int ImageProcessor::m_counter { 0 };


ImageProcessor::ImageProcessor(QObject *parent) : QObject(parent)
{

    connect (this, &ImageProcessor::start, this, &ImageProcessor::encode);

    connect (this, &ImageProcessor::ffmpegFinished, [&](QString errorString)
    {
        if (errorString == "") {
            emit finished(true);
        } else {
            emit finished(false);
            //TODO: Logging here

            QFile logger(QApplication::applicationDirPath() + "/Log/imageprocessor.log");
            logger.open(QIODevice::Append | QIODevice::Text);
            QTextStream stream;
            stream.setDevice(&logger);
            stream << "--- " << QDateTime::currentDateTime().toString() << " ---" "\n\n" << errorString << "\n";
            logger.close();

        }

        m_buffer.clear();

    });

}

void ImageProcessor::capture(QImage input)
{

    /* Get and process image from Qt */


    auto t0 = std::chrono::steady_clock::now();

    saveToBuffer(input);

    auto t1 = std::chrono::steady_clock::now();

    auto interval = t1 - t0;

    auto dur = std::chrono::duration<double, milli>(interval).count();

    Q_UNUSED(dur)

    m_counter++;

    //    qDebug() << "FFMPEG:" << static_cast<uint>(dur);

}

void ImageProcessor::save()
{    
    QFile::remove(QApplication::applicationDirPath() + "/Temp/~temp.mov");
    QFuture<void> f = QtConcurrent::run(this, &ImageProcessor::processBuffer);
}

void ImageProcessor::initialize(SwfSettings settings)
{
    initialize (settings.WorkingFolder,
                settings.OutFile,
                settings.AudioFile,
                settings.Size,
                settings.Color,
                settings.Method,
                settings.FrameRate,
                settings.Duration,
                settings.VideoCodec,
                settings.Compressed,
                settings.FillWhite);
}

void ImageProcessor::initialize(QString path,
                                QString outFile,
                                QString audioFile,
                                QSize outSize,
                                QColor transparentColor,
                                ProcessMethod method,
                                uint fps,
                                uint duration,
                                QString videoCodec,
                                bool compressed,
                                bool fillWhite)
{

    m_counter = 0;

    m_fillWhite = fillWhite;

    m_buffer.clear();

    m_buffer.reserve(300);

    ticks.clear();

    m_path = path;

    m_size.width = outSize.width();

    m_size.height = outSize.height();

    m_method = method;

    m_outFile = outFile;

    m_audioFile = audioFile;

    m_fps = fps;

    m_duration = duration;

    m_videoCodec = videoCodec;

    m_transparentColor = transparentColor;

    QFile::remove(m_path + "/" + outFile);

    QFileInfo fileInfo(m_outFile);

    //TODO: Remove everything in Temp folder
    QFile::remove(QApplication::applicationDirPath() + "/Temp/~temp.mov");

    if (m_method == ProcessMethod::OPENCV) {

        /* Initialize writer */

        m_writer = shared_ptr<VideoWriter>(new VideoWriter((m_path + "/temp.avi").toStdString(),
                                                           CV_FOURCC_PROMPT,
                                                           30,
                                                           cv::Size(m_size.width, m_size.height)
                                                           ));

        return;

    }

    /* Default method is FFMPEG */

    m_method = ProcessMethod::FFMPEG;

    m_compressed = compressed;

    QDir dir (m_path);

    for (auto entry: dir.entryInfoList()) {
        if (entry.suffix() == "png") {
            dir.remove(entry.filePath());
        }
    }

}

void ImageProcessor::on_Release()
{

    if (m_path.isEmpty()) {
        return;
    }

    QDir dir (m_path);

    for (auto entry: dir.entryInfoList()) {
        if (entry.suffix() == "png") {
            dir.remove(entry.filePath());
        }
    }

    destroyAllWindows();

}


Mat ImageProcessor::toTransparent(Mat input)
{

    auto b = m_transparentColor.blue();

    auto g = m_transparentColor.green();

    auto r = m_transparentColor.red();

    /* Set transparency in alpha channel on the pure green area */

    Mat channels[4];

    Mat mask;

    split(input, channels);

    inRange(input, Scalar(b - 1, g - 1, r - 1), Scalar(b, g, r), mask);

    bitwise_not(mask, channels[3]);

    //imshow("img", channels[3]);

    if (m_fillWhite) {

        Mat whiteBackground = Mat::ones(channels[0].size(), channels[0].type()) * 255;

        for (uint i = 0; i < 3; ++i) {
            whiteBackground.copyTo(channels[i], mask);
        }

    }

    Mat rgba;

    cv::merge(channels, 4, rgba);

    return rgba;

}

void ImageProcessor::saveToBuffer(QImage input)
{

    auto image = input.copy();

    Mat mat (image.height(), image.width(), CV_8UC4, static_cast<uchar*>(image.bits()), image.bytesPerLine());

    cvtColor(mat, mat, CV_RGBA2RGB);

    cv::resize(mat, mat, m_size);

    vector<uchar> imageBuffer;

    imencode(".bmp", mat, imageBuffer);

    m_buffer.push_back(imageBuffer);

}

void ImageProcessor::processBuffer()
{
    uint counter { 0 };

    for (auto img: m_buffer) {

        processImage(cv::imdecode(img, CV_LOAD_IMAGE_COLOR), counter);

        ++counter;
    }

    emit start();

}

void ImageProcessor::processImage(Mat inputImage, uint counter)
{

    auto number = QString("%1").arg(counter, 5, 10, QChar('0'));

    auto out = toTransparent(inputImage);

    imwrite((m_path + "/" + number + ".png").toStdString(), out);

}

void ImageProcessor::encode()
{
    if (m_method == ProcessMethod::OPENCV) {

        return;

    }

    m_process =  new QProcess;

    QObject::connect(m_process,
                     static_cast<void (QProcess::*) (int, QProcess::ExitStatus)> (&QProcess::finished),
                     this, [&](int exitCode,
                     QProcess::ExitStatus exitStatus)
    {

        qDebug() << "ffmpeg proc fin" << m_outFile;
        if (exitCode == 0) {
            qDebug() << "FFMPEG finished";
            emit ffmpegFinished("");
        } else {

            qDebug() << QString(m_process->readAll());
            emit ffmpegFinished(QString(m_process->readAllStandardError()));
        }

        Q_UNUSED(exitStatus);

        QDir dir (m_path);

        for (auto entry: dir.entryInfoList()) {
            if (entry.suffix() == "png") {
                dir.remove(entry.filePath());
            }
        }

    });

    /* Default process method is FFMPEG */

    auto ffmpegCommand = QString("\"%1/ffmpeg/ffmpeg.exe \"").arg(QApplication::applicationDirPath());

    auto arguments = ffmpegArgs();

    m_process->kill();

    qDebug() << "Starting ffmpeg";
    qDebug() << arguments;

//    m_process->start(ffmpegCommand + arguments);
    m_process->start("ffmpeg.exe",ffmpegArgs());

//    qDebug() << "started" << m_process->state();
    m_process->waitForFinished();

qDebug() << "ffmpeg proc fin" << m_process->readAllStandardError();
}

QStringList ImageProcessor::ffmpegArgs()
{

    /* Compensate frames dropped be fps decreasing */
    auto factor = m_counter / ( m_duration / (1000.0/m_fps) );

    //qDebug() << "FPS factor:" << factor;

    QStringList arguments;

    QString audioArg;
    if (m_audioFile != "") {

        audioArg = QString ( " -i \"%1\" -af afade=t=out:st=%2:d=2:curve=hsin -shortest " ).arg(m_audioFile).arg(QString::number(m_duration/1000 - 2));
    } else {
        audioArg = "";
    }

    if (m_compressed) {
        arguments << "-framerate"
                  << QString::number(m_fps * factor)
                  << "-start_number"
                  << "00000"
                  << "-i"
                  << "" + m_path + "/%5d.png";
                  if(!audioArg.isEmpty())
                    arguments << audioArg;

                  arguments << "-vcodec"
                  << m_videoCodec
                  << "-qscale:v"
                  << "1"
                  << m_outFile.replace("% 5", "%5");

//        arguments = QString(" -framerate %1 -start_number 00000 -i \"%2/% 5d.png\" %3 -vcodec %4 -qscale:v 1 \"%5\"").
//                arg(m_fps * factor).arg(m_path).arg(audioArg).arg(m_videoCodec).arg(m_outFile).replace("% 5", "%5");

        /* Compressed */

    } else {

        arguments << "-loglevel panic"
                  << "-framerate"
                  << QString::number(m_fps * factor)
                  << "-f"
                  << "image2"
                  << "-start_number"
                  << "00000"
                  << "-i"
                  << m_path + "% 5d.png"
                  << audioArg
                  << "-vcodec"
                  << "rawvideo"
                  << m_outFile.replace("% 5", "%5");
//        arguments = QString(" -loglevel panic -framerate %1 -start_number 00000 -i \"%2/% 5d.png\" %3 -vcodec rawvideo \"%4\"").
//                arg(m_fps * factor).arg(m_path).arg(m_audioFile).arg(m_outFile).replace("% 5", "%5");

    }

    //qDebug() << arguments;

    return arguments;
}

