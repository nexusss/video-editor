#-------------------------------------------------
#
# Project created by QtCreator 2016-04-10T22:27:35
#
#-------------------------------------------------

QT       += widgets xml multimedia axcontainer

TARGET = SwfLib
TEMPLATE = lib

DEFINES += SWFLIB_LIBRARY

SOURCES += swflib.cpp \
        imageprocessor.cpp \
        xmlparser.cpp \
    swfsettings.cpp \


HEADERS += swflib.h \
        swflib_global.h \
        imageprocessor.h \
        xmlparser.h \
    swfsettings.h \


#INCLUDEPATH += d:/libs/opencv31/opencv/build/include

LIBS += -lgdi32
LIBS += -lUser32

macx|win32: LIBS += -L$$PWD/../x86/vc12/lib/ -lopencv_world300

INCLUDEPATH += $$PWD/../include
DEPENDPATH += $$PWD/../include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../x86/vc12/lib/opencv_world300.lib
else:macx|win32-g++: PRE_TARGETDEPS += $$PWD/../x86/vc12/lib/libopencv_world300.a
